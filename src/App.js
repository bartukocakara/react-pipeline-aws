
import './App.css';
// import routes from './config/routes.js';
import {BrowserRouter} from 'react-router-dom';
import Routes from "./Routes";
import { store } from "./createStore"
import { Provider } from "react-redux"

function App() {

  // const routeComponents = routes.map(({path, component, exact}, key) => <Route exact={exact} path={path} component={component} key={key} />);

  return (
    <Provider store={store}>
      <BrowserRouter>
        
          <Routes />
        
      </BrowserRouter>
    </Provider>
  );
}

export default App;
