import {Route, Redirect} from "react-router-dom";
import React from "react";

const LogoutLayout = ({children, props, ...rest}) => {
    
    const login = localStorage.getItem("crm-token") ? true : false;
    return (
        <Route
            {...rest}
            render={() => {
                return login ? (
                    <>
                    <div className="App">
                        <div class="app-content content p-5">
                            {children}
                        </div>
                    </div>
                    </>
                ) : (
                    <Redirect
                        to={{
                            pathname: "/login",
                            state: {notSignedIn: true},
                        }}
                    />
                );
            }}
        />
    );
};
export default LogoutLayout;

