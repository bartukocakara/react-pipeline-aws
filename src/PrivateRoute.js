import {Route, Redirect} from "react-router-dom";
import React from "react";
import Navbar from './components/layouts/Navbar';
import Sidebar from './components/layouts/Sidebar';

const PrivateRoute = ({children, ...rest}) => {
    
    const login = localStorage.getItem("crm-token") ? true : false;

    return (
        <Route
            {...rest}
            render={() => {
                return login ? (
                    <>
                        <div className="App">
                            <Navbar />
                            <Sidebar />
                                {children}
                        </div>
                    </>
                ) : (
                    <Redirect
                        to={{
                            pathname: "/login",
                            state: {login: true},
                        }}
                    />
                );
            }}
        />
    );
};
export default PrivateRoute;

