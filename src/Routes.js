import React from 'react'

import {Switch, Route} from "react-router-dom" 
import Login from './views/Auth/Login'
import Register from './views/Auth/Register'
import Home from "./views/Home";
// import Profile from "./views/Profile";
import Sales from "./views/Sales";
import SalesDetail from "./views/Sales/Show";
import Customers from "./views/Customers";
import OrderTrackings from "./views/OrderTrackings";
import OrderTrackingsDetail from "./views/OrderTrackings/Show";
import CustomersDetail from "./views/Customers/Show";
import Email from "./views/Email";
import PrivateRoute from './PrivateRoute'
import Logout from './views/Auth/Logout'
import LogoutLayout from './LogoutLayout'

const Routes = () => {
  return (  
    <Switch>
        <Route exact path="/" component={Login} />
        <PrivateRoute path="/login" component={Login} />
        <Route path="/register" component={Register} />
        <PrivateRoute path='/home' exact>
            <Home />
        </PrivateRoute>
        <PrivateRoute path='/sales' exact>
            <Sales />
        </PrivateRoute>
        <PrivateRoute path='/sales/:id'>
            <SalesDetail />
        </PrivateRoute>
        <PrivateRoute path='/customers' exact>
            <Customers />
        </PrivateRoute>
        <PrivateRoute path='/customers/:id' exact>
            <CustomersDetail />
        </PrivateRoute>
        <PrivateRoute path='/order-trackings' exact>
            <OrderTrackings />
        </PrivateRoute>
        <PrivateRoute path='/order-trackings/:id' exact>
            <OrderTrackingsDetail />
        </PrivateRoute>
        <PrivateRoute path='/crm/email'>
            <Email />
        </PrivateRoute>


        <LogoutLayout path="/logout" component={Logout} exact />
    </Switch>
  )
}

export default Routes
