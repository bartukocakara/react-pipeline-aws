import axios from "axios"
import { debounce } from "lodash"
import React, { useEffect, useRef, useState } from "react"
import Paginator from "./Paginator"
import HttpService from "../../../services/HttpService";
import "./dataTables.bootstrap5.min.css"
const SORT_ASC = "asc"
const SORT_DESC = "desc"

const DataTable = ({ columns, fetchUrl, tableActions, status = null }) => {
    const [data, setData] = useState([])
    const [perPage, setPerPage] = useState(10)
    const [sortColumn, setSortColumn] = useState(columns[0]["label"])
    const [sortOrder, setSortOrder] = useState("asc")
    const [search, setSearch] = useState("")
    const [pagination, setPagination] = useState({})
    const [currentPage, setCurrentPage] = useState(1)

    const [loading, setLoading] = useState(true)
    
    const handleSort = (column) => {
        if (column === sortColumn) {
            sortOrder === SORT_ASC ? setSortOrder(SORT_DESC) : setSortOrder(SORT_ASC)
        } else {
            setSortColumn(column)
            setSortOrder(SORT_ASC)
        }
    }

    const handleSearch = useRef(
        debounce((query) => {
            setSearch(query)
            setCurrentPage(1)
            setSortOrder(SORT_ASC)
            setSortColumn(columns[0])
        }, 500)
    ).current

    const handlePerPage = (perPage) => {
        setCurrentPage(1)
        setPerPage(perPage)
    }

    const loaderStyle = { width: "2rem", height: "2rem" }

    useEffect(() => {
        const fetchData = async () => {
            setLoading(true)
            const params = {
                sort_field:sortColumn,
                sort_order:sortOrder,
                search,
                per_page:perPage,
                page:currentPage,
                status:2,
            }
            const http = new HttpService()
            const token = "Bearer " + localStorage.getItem("crm-token");
            const url = http.url + fetchUrl
            const headers = { 
                'Authorization': token,
            };
            await axios({url , params, headers }).then(
                response => response.data 
            ).then(
                response => {
                    if(response.status_code === 200)
                    {
                        setData(response.data.data)
                        setPagination(response.data)
                        setLoading(false)
                    }
                    else {
                        setLoading(false)
                    }
                }
            ).catch((error) => {
                console.log(error)
            })
        }

        fetchData()
    }, [perPage, sortColumn, sortOrder, search, currentPage, fetchUrl])
    return (
        <div>
            {/* Search per page starts */}
            <div className="row mb-3  p-2">
                <div className="col-md-3">
                    <div className="input-group">
                        <input
                            className="form-control"
                            placeholder="Search..."
                            type="search"
                            onChange={(e) => handleSearch(e.target.value)}
                        />
                    </div>
                </div>
                <div className="col-md-2">
                    <div className="input-group">
                        <label className="mt-2 me-2">Per page</label>
                        <select className="form-select" value={perPage} onChange={(e) => handlePerPage(e.target.value)}>
                            <option value="5">5</option>
                            <option value="10">10</option>
                            <option value="20">20</option>
                            <option value="50">50</option>
                        </select>
                    </div>
                </div>
            </div>
            {/* Search per page ends  */}
            <div className="tableFixHead">
                <table className="table table-hover">
                    <thead>
                        <tr>
                            {columns.map((column, i) => {
                                return (
                                    <th key={i} onClick={(e) => handleSort(column)}>
                                        {column["label"].toUpperCase().replace("_", " ")}
                                        {column["datas"].includes(sortColumn) ? (
                                            <span>
                                                {sortOrder === SORT_ASC ? (
                                                    <i className="ms-1 fa fa-arrow-up" aria-hidden="true"></i>
                                                ) : (
                                                    <i className="ms-1 fa fa-arrow-down" aria-hidden="true"></i>
                                                )}
                                            </span>
                                        ) : null}
                                    </th>
                                )
                            })}
                            <th>
                                Actions
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        {data.length === 0 ? (
                            <tr>
                                <td colSpan={columns.length}>No items found</td>
                            </tr>
                        ) : (
                            ""
                        )}

                        {!loading ? (
                            data.map((d, index) => {
                                return (
                                    <tr key={index}>
                                        {
                                            columns.map((column, i) => {
                                                return (
                                                        <td key={i}>
                                                                {column["datas"].map((data, index) => {
                                                                        if(Array.isArray(d[column["label"]][data])) {
                                                                            return  (
                                                                                        <li key={index}> 
                                                                                            {
                                                                                                d[column["label"]][data].map((v, i) => {
                                                                                                    console.log(v)
                                                                                                    return (
                                                                                                        v
                                                                                                    )
                                                                                                })
                                                                                            }
                                                                                        </li>
                                                                                    )
                                                                        }
                                                                        else{
                                                                                return (
                                                                                        <li> 
                                                                                            {
                                                                                                d[column["label"]][data]
                                                                                            }
                                                                                        
                                                                                        </li>
                                                                                    )
                                                                        }
                                                                        
                                                                        } 
                                                                    )
                                                                }
                                                        </td>
                                                    )
                                                }
                                            )
                                        }
                                        <td>
                                            <div className="dropdown">
                                                <button type="button" 
                                                        className="btn btn-sm dropdown-toggle hide-arrow waves-effect waves-float waves-light" 
                                                        data-bs-toggle="dropdown">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-more-vertical"><circle cx="12" cy="12" r="1"></circle><circle cx="12" cy="5" r="1"></circle><circle cx="12" cy="19" r="1"></circle></svg>
                                                </button>
                                                <div className="dropdown-menu">
                                                    {
                                                        tableActions.map((item, i) => (
                                                            <button className="dropdown-item" value={item.value} onClick={(e) => alert(item.value)}>
                                                                <span className="m-1">
                                                                {item.icon}
                                                                </span>
                                                                <span>{item.label}</span>
                                                            </button>
                                                        ))
                                                    }
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                )
                            })
                        ) : (
                            <tr>
                                <td colSpan={columns.length + 1}>
                                    <div className="d-flex justify-content-center">
                                        <div className="spinner-border" style={loaderStyle} role="status">
                                            <span className="sr-only"></span>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        )}
                    </tbody>
                </table>
            </div>
            {data.length > 0 && !loading ? (
                <div className="mt-2">
                    <Paginator
                        pagination={pagination}
                        pageChanged={(page) => setCurrentPage(page)}
                        totalItems={data.length}
                    />
                </div>
            ) : null}
        </div>
    )
}

export default DataTable
