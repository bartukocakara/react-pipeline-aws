import { Link } from "react-router-dom"
import { Messages } from './Navbar/Messages';
import NavbarLeft from "./Navbar/NavbarLeft";
import { Notifications } from './Navbar/Notifications';

const Navbar = () => {
    
    return (
        <>
        <nav className="header-navbar navbar navbar-expand-lg align-items-center floating-nav navbar-light navbar-shadow container-xxl">
            <div className="navbar-container d-flex content">
                <div className="bookmark-wrapper d-flex align-items-center">
                <div class="bookmark-wrapper d-flex align-items-center">

          <NavbarLeft />
        </div>
                <ul className="nav navbar-nav d-xl-none">
                    <li className="nav-item">
                        <p className="nav-link menu-toggle">
                            <i className="ficon" data-feather="menu"></i>
                        </p>
                    </li>
                </ul>
                </div>
                <ul className="nav navbar-nav align-items-center ms-auto">
                    <li className="nav-item dropdown dropdown-language">
                        <p className="nav-link dropdown-toggle" id="dropdown-flag" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i className="flag-icon flag-icon-us"></i>
                            <span className="selected-language">English</span></p>
                        <div className="dropdown-menu dropdown-menu-end" aria-labelledby="dropdown-flag"><Link className="dropdown-item" data-language="en"><i className="flag-icon flag-icon-us"></i> English</Link><Link className="dropdown-item" data-language="fr"><i className="flag-icon flag-icon-fr"></i> French</Link><Link className="dropdown-item" data-language="de"><i className="flag-icon flag-icon-de"></i> German</Link><Link className="dropdown-item" data-language="pt"><i className="flag-icon flag-icon-pt"></i> Portuguese</Link></div>
                    </li>
                    <li className="nav-item d-none d-lg-block">
                        <p className="nav-link nav-link-style">
                            <i className="ficon" data-feather="moon"></i>
                        </p>
                    </li>
                    
                    <Messages />
                    <Notifications />
                    <li className="nav-item dropdown dropdown-user">
                        <Link className="nav-link dropdown-toggle dropdown-user-link" id="dropdown-user" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <div className="user-nav d-sm-flex d-none"><span className="user-name fw-bolder">John Doe</span><span className="user-status">Admin</span></div><span className="avatar"><img className="round" src="/app-assets/images/portrait/small/avatar-s-11.jpg" alt="avatar" height="40" width="40" /><span className="avatar-status-online"></span></span></Link>
                        <div className="dropdown-menu dropdown-menu-end" aria-labelledby="dropdown-user">
                            <Link className="dropdown-item" to="profile"><i className="me-50" data-feather="user"></i> Profile</Link>
                            <Link to="/logout" className="dropdown-item"><i className="me-50" data-feather="power"></i> Logout</Link>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
        </>
    )
}

export default Navbar
