import React from 'react'
import { Link } from 'react-router-dom'

const NavbarLeft = () => {
  return (
        <ul className="nav navbar-nav bookmark-icons">
            <li className="nav-item d-none d-lg-block">
                <Link className="nav-link" to={"/crm/email"}  >
                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-mail ficon">
                    <path d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z"></path><polyline points="22,6 12,13 2,6"></polyline>
                    </svg>
                </Link>
            </li>
        </ul>
  )
}

export default NavbarLeft