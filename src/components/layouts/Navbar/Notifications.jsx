import React from 'react'
import { Link } from 'react-router-dom'

export const Notifications = () => {
  return (
            <li className="nav-item dropdown dropdown-notification me-25"><Link className="nav-link" href="#" data-bs-toggle="dropdown"><i className="ficon" data-feather="bell"></i><span className="badge rounded-pill bg-danger badge-up">5</span></Link>
                <ul className="dropdown-menu dropdown-menu-media dropdown-menu-end">
                    <li className="dropdown-menu-header">
                        <div className="dropdown-header d-flex">
                        <h4 className="notification-title mb-0 me-auto">Notifications</h4>
                        <div className="badge rounded-pill badge-light-primary">6 New</div>
                        </div>
                    </li>
                    <li className="scrollable-container media-list"><Link className="d-flex" href="#">
                        <div className="list-item d-flex align-items-start">
                            <div className="me-1">
                            <div className="avatar"><img src="/app-assets/images/portrait/small/avatar-s-15.jpg" alt="avatar" width="32" height="32" /></div>
                            </div>
                            <div className="list-item-body flex-grow-1">
                            <p className="media-heading"><span className="fw-bolder">Congratulation Sam 🎉</span>winner!</p><small className="notification-text"> Won the monthly best seller badge.</small>
                            </div>
                        </div></Link><Link className="d-flex" href="#">
                        <div className="list-item d-flex align-items-start">
                            <div className="me-1">
                            <div className="avatar"><img src="/app-assets/images/portrait/small/avatar-s-3.jpg" alt="avatar" width="32" height="32" /></div>
                            </div>
                            <div className="list-item-body flex-grow-1">
                            <p className="media-heading"><span className="fw-bolder">New message</span>&nbsp;received</p><small className="notification-text"> You have 10 unread messages</small>
                            </div>
                        </div></Link><Link className="d-flex" href="#">
                        <div className="list-item d-flex align-items-start">
                            <div className="me-1">
                            <div className="avatar bg-light-danger">
                                <div className="avatar-content">MD</div>
                            </div>
                            </div>
                            <div className="list-item-body flex-grow-1">
                            <p className="media-heading"><span className="fw-bolder">Revised Order 👋</span>&nbsp;checkout</p><small className="notification-text"> MD Inc. order updated</small>
                            </div>
                        </div></Link>
                        <div className="list-item d-flex align-items-center">
                        <h6 className="fw-bolder me-auto mb-0">System Notifications</h6>
                        <div className="form-check form-check-primary form-switch">
                            <input className="form-check-input" id="systemNotification" type="checkbox"/>
                            <label className="form-check-label" ></label>
                        </div>
                        </div><Link className="d-flex" href="#">
                        <div className="list-item d-flex align-items-start">
                            <div className="me-1">
                            <div className="avatar bg-light-danger">
                                <div className="avatar-content"><i className="avatar-icon" data-feather="x"></i></div>
                            </div>
                            </div>
                            <div className="list-item-body flex-grow-1">
                            <p className="media-heading"><span className="fw-bolder">Server down</span>&nbsp;registered</p><small className="notification-text"> USA Server is down due to high CPU usage</small>
                            </div>
                        </div></Link><Link className="d-flex" href="#">
                        <div className="list-item d-flex align-items-start">
                            <div className="me-1">
                            <div className="avatar bg-light-success">
                                <div className="avatar-content"><i className="avatar-icon" data-feather="check"></i></div>
                            </div>
                            </div>
                            <div className="list-item-body flex-grow-1">
                            <p className="media-heading"><span className="fw-bolder">Sales report</span>&nbsp;generated</p><small className="notification-text"> Last month sales report generated</small>
                            </div>
                        </div></Link><Link className="d-flex" href="#">
                        <div className="list-item d-flex align-items-start">
                            <div className="me-1">
                            <div className="avatar bg-light-warning">
                                <div className="avatar-content"><i className="avatar-icon" data-feather="alert-triangle"></i></div>
                            </div>
                            </div>
                            <div className="list-item-body flex-grow-1">
                            <p className="media-heading"><span className="fw-bolder">High memory</span>&nbsp;usage</p><small className="notification-text"> BLR Server using high memory</small>
                            </div>
                        </div></Link>
                    </li>
                    <li className="dropdown-menu-footer"><Link className="btn btn-primary w-100">Read all notifications</Link></li>
                </ul>
            </li>
  )
}
