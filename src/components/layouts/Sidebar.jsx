import React from 'react'

import {
    Link
} from "react-router-dom";

import menuItems from '../../config/menu.js';

const Sidebar = () => {

    console.log(menuItems[1]["name"])
    return (
        <>
            <div className="main-menu menu-fixed menu-light menu-accordion menu-shadow expanded" data-scroll-to-active="true" style={{ touchAction: "none", userSelect: "none", userDrag: "none", tapHighlightColor: "rgba(0, 0, 0, 0)" }}>
                <div className="navbar-header">
                    <ul className="nav navbar-nav flex-row">
                    <li className="nav-item me-auto"><Link className="navbar-brand" href="../../../html/ltr/vertical-menu-template/index.html"><span className="brand-logo">
                           </span>
                        <h2 className="brand-text">Lidyana Crm</h2></Link></li>
                    <li className="nav-item nav-toggle"><Link className="nav-link modern-nav-toggle pe-0" data-bs-toggle="collapse"><i className="d-block d-xl-none text-primary toggle-icon font-medium-4" data-feather="x"></i><i className="d-none d-xl-block collapse-toggle-icon font-medium-4  text-primary" data-feather="disc" data-ticon="disc"></i></Link></li>
                    </ul>
                </div>
                <div className="shadow-bottom"></div>
                <div className="main-menu-content">
                    <ul className="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
                        {
                            menuItems.map(menuItem => (
                                <li className="nav-item">
                                    <Link className="d-flex align-items-center" to={menuItem.to} >
                                        { menuItem.dataFeather }
                                        <span className="menu-title text-truncate" data-i18n={menuItem.datai18}>{ menuItem.name }</span>
                                    </Link>
                                    {menuItem.children ?
                                        <ul className="menu-content">
                                            {
                                                menuItem.children.map((childMenu, i) => (
                                                <li  className="d-flex justify-space-between"  key={i}>
                                                    { childMenu.dataFeather }
                                                    <Link data-i18n="List" to={childMenu.to}>{childMenu.name}</Link>
                                                </li>
                                                ))
                                                
                                                
                                            }
                                        </ul>
                                        :
                                        null  
                                      }

                                </li>
                            ))
                        }
                    </ul>
                </div>
            </div>
        </>
    )
}

export default Sidebar
