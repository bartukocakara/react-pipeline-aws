// import { AiFillStar } from "react-icons/ai";
// import { AiOutlineFileSearch, AiOutlineSearch } from "react-icons/ai";
// import {FaBasketballBall} from "react-icons/fa";
// import {BsPeopleFill} from "react-icons/bs";
// import {ImTree} from "react-icons/im";
// import {GiStrong} from "react-icons/gi";
// import {BsFillCameraVideoFill} from "react-icons/bs";
// import {GiRainbowStar, GiBasketballBasket} from "react-icons/gi";
// import {FaRunning} from "react-icons/fa";
// import {RiTeamLine} from "react-icons/ri";
// import {GoPerson} from "react-icons/go";
import {FaUserTie} from "react-icons/fa";
import {BsFillBriefcaseFill} from "react-icons/bs";


const menuItems = [
    {   
        name : "Crm",
        // dataFeather : <AiOutlineSearch />,
        active : false,
        datai18 : "Search",
        children : [
            {
                name : "Sales",
                datai18 : "Match Search",
                to  : "/sales",
                // dataFeather : <FaBasketballBall className="m-auto" size={18} />,
                active : false,
                modalMethod : ""
            },
            {
                name : "Products",
                datai18 : "Products",
                to  : "/sales",
                // dataFeather : <FaBasketballBall className="m-auto" size={18} />,
                active : false,
                modalMethod : ""
            },
            {
                name : "Order Trackings",
                datai18 : "Order Trackings",
                to  : "/order-trackings",
                // dataFeather : <GiBasketballBasket className="m-auto" size={18} />,
                active : false,
                modalMethod : ""
            },
            {
                name : "Customers",
                datai18 : "customers",
                to  : "/customers",
                // dataFeather : <FaBasketballBall className="m-auto" size={18} />,
                active : false,
                modalMethod : ""
            },
        ]
    },
    {   
        name : "Something Else",
        // dataFeather : <AiFillStar />,
        active : false,
        datai18 : "Favorite",
        children : [
            {
                name : "Something Else 2",
                datai18 : "Something Else 2",
                active : false
            },
            {
                name : "Something Else 3",
                datai18 : "Something Else 3",
                active : false
            },
        ]
    },
    {   
        to  : "/employees",
        name : "Employees",
        dataFeather : <FaUserTie />,
        active : false,
        datai18 : "Employees",
        newTarget : false,
        subFeather : null,
        borderTop : false

    },
    {   
        to  : "/roles-permissions",
        name : "Roles Permissions",
        dataFeather : <BsFillBriefcaseFill />,
        active : false,
        datai18 : "Roles Permissions",
        newTarget : false,
        subFeather : null,
        borderTop : false

    },
];

export default menuItems