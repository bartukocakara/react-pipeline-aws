import Players from "../views/Players";
import Customers from "../views/Customers";
import OrderTrackings from "../views/OrderTrackings";
import Brands from "../views/Brands";
import Sales from "../views/Sales";
import Products from "../views/Products";

const routes = [

    { name  : "Players", path : "/players", exact : false, component : Players},
    { name  : "Customers", path : "/customers", exact : false, component : Customers},
    { name  : "Brands", path : "/brands", exact : false, component : Brands},
    { name  : "Order Trackings", path : "/order-trackings", exact : false, component : OrderTrackings},
    { name  : "Sales", path : "/sales", exact : false, component : Sales},
    { name  : "Products", path : "/products", exact : false, component : Products},
    
];

export default routes