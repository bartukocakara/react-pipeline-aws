import HttpService from "./HttpService"

export const LoadShowData = (url, id) => {
        const http = new HttpService();
        let uri = url + "/" + id;
        const tokenId = "crm-token";
        return http.getData(uri, tokenId).then(data => {
            return data;
        }).catch((error) => {
            console.error(error);
            return error;
        });
}