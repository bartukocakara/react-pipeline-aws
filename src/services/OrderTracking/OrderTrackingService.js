import HttpService from "../HttpService"

export const LoadShowCustomer = (id) => {
        const http = new HttpService();
        let showCustomerUrl = "order-trackings/" + id;
        const tokenId = "crm-token";

        return http.getData(showCustomerUrl, tokenId).then(data => {
            return data;
        }).catch((error) => {
            console.error(error);
            return error;
        });
}