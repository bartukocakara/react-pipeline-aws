import HttpService from "./../HttpService"

export const LoadShowSale = (id) => {
        const http = new HttpService();
        let showSaleUrl = "sales/" + id;
        const tokenId = "crm-token";

        return http.getData(showSaleUrl, tokenId).then(data => {
            return data;
        }).catch((error) => {
            console.error(error);
            return error;
        });
}