import HttpService from "./../HttpService"

export const LoadUserProfile = () => {
        const http = new HttpService();
        let showProfileUrl = "me";
        const tokenId = "crm-token";

        return http.getData(showProfileUrl, tokenId).then(data => {
            return data;
        }).catch((error) => {
            console.error(error);
            return error;
        });
}