import {LoadShowData} from '../../services/DataShowService'


export const loadShowAction = (fetchUrl, id) =>{
    return (dispatch)=>{
        dispatch({type:'LOADING'});

        LoadShowData(fetchUrl, id).then((res)=>{
            if(res.hasOwnProperty('status_code') && res.status_code === 200){
                 
                dispatch({type:'LOAD_DATA_SHOW_SUCCESS',res});
                    
            }else if(res.hasOwnProperty('status_code') && res.status_code > 300) { 
                dispatch({type:'LOAD_DATA_SHOW_ERROR',res})
            }
        },
        error=>{
            dispatch({type:'CODE_ERROR',error});
        }
        )
    }
    
}