import {LoadUserProfile} from '../../../services/User/ProfileService'


export const loadUserAction = (props) =>{
    return (dispatch)=>{
        dispatch({type:'LOADING'});

        LoadUserProfile().then((res)=>{
            if(res.hasOwnProperty('status_code') && res.status_code === 200){
                 
                dispatch({type:'LOAD_PROFILE_SUCCESS',res});
                
            }else if(res.hasOwnProperty('status_code') && res.status_code > 300) { 
                dispatch({type:'LOAD_PROFILE_ERROR',res})
            }
        },
        error=>{
            dispatch({type:'CODE_ERROR',error});
        }
        )
    }
    
}