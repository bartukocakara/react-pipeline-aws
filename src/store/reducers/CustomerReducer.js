const initState = {
    customerShow:""
    }
     

    const CustomerShowReducer = (state=initState, action) =>{
            switch(action.type){
            
            case 'LOADING':
                    return {
                        ...state,
                        customerShow:'loading'
                    }

            case 'LOAD_DATA_SHOW_SUCCESS':
                    return {
                        ...state,
                        customerShow:action.res,
                    }
    
            case 'LOAD_DATA_SHOW_ERROR':

                    return {
                        ...state,
                        customerShow:action.res,
                    }
    
            case 'CODE_ERROR':
                            console.log(action)
                    return {
                        ...state,
                        customerShow:'there seems to be a problem please refresh your browser',
                    }
            default:
                    return state
        }
    }
    
    export default CustomerShowReducer;