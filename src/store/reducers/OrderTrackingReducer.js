const initState = {
    orderTrackingShow:""
    }
     

    const OrderTrackingShowReducer = (state=initState, action) =>{
            switch(action.type){
            
            case 'LOADING':
                    return {
                        ...state,
                        orderTrackingShow:'loading'
                    }

            case 'LOAD_DATA_SHOW_SUCCESS':
                    return {
                        ...state,
                        orderTrackingShow:action.res,
                    }
    
            case 'LOAD_DATA_SHOW_ERROR':

                    return {
                        ...state,
                        orderTrackingShow:action.res,
                    }
    
            case 'CODE_ERROR':
                            console.log(action)
                    return {
                        ...state,
                        orderTrackingShow:'there seems to be a problem please refresh your browser',
                    }
            default:
                    return state
        }
    }
    
    export default OrderTrackingShowReducer;