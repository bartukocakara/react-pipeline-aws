import {combineReducers} from 'redux'
import AuthReducer from './User/AuthReducer'
import ProfileReducer  from './User/ProfileReducer'
import SaleReducer from './SaleReducer'
import CustomerShowReducer from './CustomerReducer'
import DataShowReducer from './DataShowReducer'

const RootReducer = combineReducers({
  userAuth : AuthReducer,
  sale : SaleReducer,
  userProfile: ProfileReducer,
  saleShow: SaleReducer,
  customerShow: CustomerShowReducer,
  dataShow: DataShowReducer,
})

export default RootReducer