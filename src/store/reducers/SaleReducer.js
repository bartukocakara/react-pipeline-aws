const initState = {
    saleShow:""
    }
     

    const SaleShowReducer = (state=initState, action) =>{
            switch(action.type){
            
            case 'LOADING':
                    return {
                        ...state,
                        saleShow:'loading'
                    }

            case 'LOAD_DATA_SHOW_SUCCESS':
                    return {
                        ...state,
                        saleShow:action.res,
                    }
    
            case 'LOAD_DATA_SHOW_ERROR':

                    return {
                        ...state,
                        saleShow:action.res,
                    }
    
            case 'CODE_ERROR':
                            console.log(action)
                    return {
                        ...state,
                        saleShow:'there seems to be a problem please refresh your browser',
                    }
            default:
                    return state
        }
    }
    
    export default SaleShowReducer;