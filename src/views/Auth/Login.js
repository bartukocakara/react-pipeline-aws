import React, {useEffect} from 'react'
import {useFormFields} from '../../helpers/hooksFormInput'
import { Link, useHistory } from 'react-router-dom'
import {useDispatch} from 'react-redux';
import {UserLoginAction} from '../../store/actions/User/AuthActions'

export default function Login(props) {

    const dispatch = useDispatch();
    const history = useHistory();

    const [fields, handleFieldChange] = useFormFields({
                                            email: "",
                                            password: ""
                                        });

      const UserLogin = (e) =>
      {
            e.preventDefault();
                
            dispatch(UserLoginAction(fields,props))
       
      } 
    
      useEffect((props) => {
        let isAuth = localStorage.getItem('crm-token')
        console.log(isAuth)
        if(isAuth && isAuth !== 'undefined') {
            console.log(props+ ":" + isAuth)
            history.push('/home')
        }
     }, [history])

    return (
        <div className="blank-page row text-center">
            <div className="app-content content col-md-6 m-auto">
                <div className="content-overlay"></div>
                    <div className="header-navbar-shadow"></div>
                    <div className="content-wrapper">
                        <div className="content-header"></div>
                            <div className="content-body">
                                <div className="auth-wrapper auth-v1 px-2">
                                <div className="auth-inner py-2">
                                <div className="card mb-0">
                                    <div className="card-body text-center">
                                    <h4 className="card-title mb-1">Welcome to Lidyana! 👋</h4>
                                    <b className="text-dark">Please sign-in to your account</b>

                                    <form className="auth-login-form mt-2"  onSubmit={UserLogin}>
                                        <div className="mb-1">
                                            <label  className="form-label">Email</label>
                                            <input type="text" 
                                                className="form-control"
                                                placeholder="john@example.com" 
                                                aria-describedby="login-email" 
                                                id="email"
                                                value = {fields.email}
                                                onChange={handleFieldChange}
                                            />
                                        </div>

                                        <div className="mb-1">
                                        <div className="d-flex justify-content-between">
                                            <label className="form-label">Password</label>
                                            <a href="page-auth-forgot-password-v1.html">
                                            <small>Forgot Password?</small>
                                            </a>
                                        </div>
                                        <div className="input-group input-group-merge form-password-toggle">
                                            <input 
                                                type="password"
                                                className="form-control form-control-merge"
                                                name="login-password"
                                                placeholder="············"
                                                aria-describedby="login-password"
                                                id="password"
                                                value = {fields.password}
                                                onChange={handleFieldChange}
                                                 />
                                            <span className="input-group-text cursor-pointer"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" className="feather feather-eye"><path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path><circle cx="12" cy="12" r="3"></circle></svg></span>
                                        </div>
                                        </div>
                                        <div className="mb-1">
                                            <div className="form-check">
                                                <input className="form-check-input" type="checkbox" id="remember-me" />
                                                <label className="form-check-label"> Remember Me </label>
                                            </div>
                                        </div>
                                        <button className="btn btn-primary w-100 waves-effect waves-float waves-light">Sign in</button>
                                    </form>
                                    <p className="text-center mt-2">
                                        <span>New on our platform?</span>
                                        <Link to="register">
                                        <span>Register</span>
                                        </Link>
                                    </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
  )
}
