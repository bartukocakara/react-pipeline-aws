import React,{useEffect} from 'react';
import {useDispatch,useSelector} from 'react-redux'
import {UserLogOutAction} from '../../store/actions/User/AuthActions'

const Logout = () => {
    const dispatch = useDispatch();
    const authResponse = useSelector(state => state.userAuth.authResponse);

    useEffect((props) => {
        dispatch(UserLogOutAction());
        console.log(authResponse);
        if( authResponse !== "" && authResponse.status_code === 200)
        {
            alert(authResponse.message);
            localStorage.removeItem('crm-token');
            props.history.push("login")
        }else if(authResponse.status_code > 300)
        {
            alert(authResponse.message);
        }
    }, [authResponse, dispatch])
  return (
    <div className="d-flex justify-content-center mt-4">
        <h5>Logging Out Bye bye !</h5>
        <div className="spinner-border" role="status">
            <span className="sr-only"></span>
        </div>
    </div>
  )
}

export default Logout