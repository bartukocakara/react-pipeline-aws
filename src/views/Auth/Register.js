import React, {useEffect} from 'react'
import {useFormFields} from '../../helpers/hooksFormInput'
import { Link, useHistory } from 'react-router-dom'
import {useDispatch} from 'react-redux';
import {signUpAction} from '../../store/actions/User/AuthActions'

export default function Resigter(props) {

    const [fields, handleFieldChange] = useFormFields({
        name:"",
        email: "",
        password: "",
        confirm :""
    });
    
    // const authResponse = useSelector(state => state.userAuth.authResponse);

    const history = useHistory();

    const ResigterUser = (e) =>
    {
        e.preventDefault();

        console.log(fields)
        const passwordMatch = checkPasswordMatch(fields.password,fields.confirm);

        if(passwordMatch === false)
        {
            alert('passwords dont match')
        return;
        }
        dispatch(signUpAction(fields));
    } 

    useEffect(() => {
        let isAuth = localStorage.getItem('crm-token')
        console.log(isAuth)
        if(isAuth && isAuth !== 'undefined') {
           history.push('/home')
        }
     }, [history])

    const dispatch = useDispatch();

    const checkPasswordMatch = (password,confirm) =>
    {
        if(password !== confirm)
        {
            return false;
        }
        return true;
    }
  return (
        <div className="blank-page row text-center">
            <div className="app-content content col-md-6 m-auto">
                <div className="content-overlay"></div>
                    <div className="header-navbar-shadow"></div>
                    <div className="content-wrapper">
                        <div className="content-header"></div>
                            <div className="content-body">
                                <div className="auth-wrapper auth-v1 px-2">
                                <div className="auth-inner py-2">
                                <div className="card mb-0">
                                    <div className="card-body text-center">
                                    <h4 className="card-title mb-1">Welcome to Lidyana! 👋</h4>
                                    <b className="text-dark">Please sign-in to your account</b>

                                    <form className="auth-Resigter-form mt-2"  onSubmit={ResigterUser}>
                                        <div className="mb-1">
                                            <label  className="form-label">Name</label>
                                            <input type="text" 
                                                className="form-control"
                                                placeholder="name" 
                                                id="name"
                                                value = {fields.name}
                                                onChange={handleFieldChange}
                                            />
                                        </div>
                                        <div className="mb-1">
                                            <label  className="form-label">Email</label>
                                            <input type="text" 
                                                className="form-control"
                                                placeholder="test@gmail.com" 
                                                id="email"
                                                value = {fields.email}
                                                onChange={handleFieldChange}
                                            />
                                        </div>
                                        <div className="mb-1">
                                            <label  className="form-label">Password</label>
                                            <input type="password" 
                                                className="form-control"
                                                id="password"
                                                value = {fields.password}
                                                onChange={handleFieldChange}
                                            />
                                        </div>
                                        <div className="mb-1">
                                            <label  className="form-label">Password Confirm</label>
                                            <input type="password" 
                                                className="form-control"
                                                id="confirm"
                                                value = {fields.confirm}
                                                onChange={handleFieldChange}
                                            />
                                        </div>
                                        <button className="btn btn-primary w-100 waves-effect waves-float waves-light">Sign up</button>
                                    </form>
                                    <p className="text-center mt-2">
                                        <span>New on our platform?</span>
                                        <Link to="login">
                                        <span>Login</span>
                                        </Link>
                                    </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
  )
}
