import React from 'react'

const Create = () => {
  return (
    <div className="modal modal-slide-in fade" id="modals-slide-in">
            <div className="modal-dialog sidebar-sm">
                <form className="add-new-record modal-content pt-0">
                    <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close">×</button>
                    <div className="modal-header mb-1">
                        <h5 className="modal-title" id="exampleModalLabel">New Record</h5>
                    </div>
                    <div className="modal-body flex-grow-1">
                        <div className="mb-1">
                            <label className="form-label" for="basic-icon-default-fullname">Full Name</label>
                            <input type="text" className="form-control dt-full-name" id="basic-icon-default-fullname" placeholder="John Doe" aria-label="John Doe" />
                        </div>
                        <div className="mb-1">
                            <label className="form-label" for="basic-icon-default-post">Post</label>
                            <input type="text" id="basic-icon-default-post" className="form-control dt-post" placeholder="Web Developer" aria-label="Web Developer" />
                        </div>
                        <div className="mb-1">
                            <label className="form-label" for="basic-icon-default-email">Email</label>
                            <input type="text" id="basic-icon-default-email" className="form-control dt-email" placeholder="john.doe@example.com" aria-label="john.doe@example.com" />
                        <small className="form-text"> You can use letters, numbers &amp; periods </small>
                        </div>
                        <div className="mb-1">
                            <label className="form-label" for="basic-icon-default-date">Joining Date</label>
                            <input type="text" className="form-control dt-date flatpickr-input" id="basic-icon-default-date" placeholder="MM/DD/YYYY" aria-label="MM/DD/YYYY" readonly="readonly" />
                        </div>
                        <div className="mb-4">
                            <label className="form-label" for="basic-icon-default-salary">Salary</label>
                            <input type="text" id="basic-icon-default-salary" className="form-control dt-salary" placeholder="$12000" aria-label="$12000" />
                        </div>
                        <button type="button" className="btn btn-primary data-submit me-1 waves-effect waves-float waves-light">Submit</button>
                        <button type="reset" className="btn btn-outline-secondary waves-effect" data-bs-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
  )
}

export default Create