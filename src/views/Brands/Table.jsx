import React from 'react'
import Create from './Create'
import TableBody from './TableBody'
import TableFooter from './TableFooter'
import TableHeader from './TableHeader'
import TableOptions from './TableOptions'

const Table = () => {
  return (
    <section id="basic-datatable">
        <div className="row">
            <div className="col-12">
                <div className="card">
                    <div id="DataTables_Table_0_wrapper" className="dataTables_wrapper dt-bootstrap5 no-footer">
                        <TableOptions />
                        <table className="datatables-basic table dataTable no-footer dtr-column" id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info" style={{ width: "1207px" }}>
                        <TableHeader />
                        <TableBody />
                        </table>
                        <TableFooter />
                    </div>
                </div>
            </div>
        </div>
        <Create />
    </section>
  )
}

export default Table