import React from 'react'

const TableBody = () => {
  return (
    <tbody>
        <tr className="odd">  
            <td valign="top" colspan="7" className="dataTables_empty">
                No data available in table
            </td>
        </tr>
    </tbody>
  )
}

export default TableBody