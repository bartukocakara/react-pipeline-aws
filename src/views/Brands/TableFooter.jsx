import React from 'react'

const TableFooter = () => {
  return (
    <>
        <div className="d-flex justify-content-between mx-0 row">
            <div className="col-sm-12 col-md-6">
                <div className="dataTables_info" id="DataTables_Table_0_info" role="status" aria-live="polite">
                    Showing 0 to 0 of 0 entries </div>
            </div>
        </div>
        <div className="col-sm-12 col-md-6">
            <div className="dataTables_paginate paging_simple_numbers" id="DataTables_Table_0_paginate">
                <ul className="pagination">
                    <li className="paginate_button page-item previous disabled" id="DataTables_Table_0_previous">
                        <a href="#" aria-controls="DataTables_Table_0" data-dt-idx="0" tabindex="0" className="page-link">&nbsp;</a>
                    </li>
                    <li className="paginate_button page-item next disabled" id="DataTables_Table_0_next">
                        <a href="#" aria-controls="DataTables_Table_0" data-dt-idx="1" tabindex="0" className="page-link">&nbsp;</a>
                    </li>
                </ul>
            </div>
        </div>
    </>
  )
}

export default TableFooter