import React from 'react'

const TableHeader = () => {
  return (
        <thead>
            <tr role="row">
                <th className="control sorting_disabled" rowspan="1" colspan="1" style={{ width: "35px", display: "none" }} aria-label="">
                </th>
                <th className="sorting_disabled dt-checkboxes-cell dt-checkboxes-select-all" rowspan="1" colspan="1" style={{ width : "35px;"}} data-col="1" aria-label="">
                    <div className="form-check"> 
                        <input className="form-check-input" type="checkbox" value="" id="checkboxSelectAll" />
                        <label className="form-check-label" for="checkboxSelectAll"></label>
                    </div>
                </th>
                <th className="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style={{width: "116px;"}} aria-label="Name: activate to sort column ascending">
                    Name
                </th>
                <th className="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style={{ width: "118px" }} aria-label="Email: activate to sort column ascending">Email</th>
                <th className="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style={{ width: "107px" }} aria-label="Date: activate to sort column ascending">Date</th>
                <th className="sorting sorting_desc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style={{ width: "138px" }} aria-label="Salary: activate to sort column ascending" aria-sort="descending">Salary</th>
                <th className="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style={{ width: "133px" }} aria-label="Status: activate to sort column ascending">Status</th>
                <th className="sorting_disabled" rowspan="1" colspan="1" style={{ width: "143px" }} aria-label="Actions">Actions</th>
            </tr>
        </thead>
  )
}

export default TableHeader