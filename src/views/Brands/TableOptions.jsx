import React from 'react'

const TableOptions = () => {
  return (
    <>
        <div className="card-header border-bottom p-1">
            <div className="head-label">
                <h6 className="mb-0">DataTable with Buttons</h6>
            </div>
            <div className="dt-action-buttons text-end">
                <div className="dt-buttons d-inline-flex me-2">
                    <button type="button" className="btn btn-outline-primary dropdown-toggle waves-effect" aria-controls="DataTables_Table_0" data-bs-toggle="dropdown" aria-expanded="false">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-share font-small-4 me-50">
                            <path d="M4 12v8a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2v-8"></path>
                            <polyline points="16 6 12 2 8 6"></polyline>
                            <line x1="12" y1="2" x2="12" y2="15"></line>
                            </svg>Export
                    </button>
                    <div className="dropdown-menu">
                        <a className="dropdown-item" href="#">Option 1</a>
                        <a className="dropdown-item" href="#">Option 2</a>
                        <a className="dropdown-item" href="#">Option 3</a>
                        <div className="dropdown-divider"></div>
                        <a className="dropdown-item" href="#">Separated link</a>
                    </div>
                </div>
                <div className="dt-buttons d-inline-flex">
                    <button className="dt-button create-new btn btn-primary" tabindex="0" aria-controls="DataTables_Table_0" type="button" data-bs-toggle="modal" data-bs-target="#modals-slide-in">
                    <span>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-plus me-50 font-small-4">
                            <line x1="12" y1="5" x2="12" y2="19"></line>
                            <line x1="5" y1="12" x2="19" y2="12">
                        </line></svg>Add New Record
                    </span>
                </button> 
                </div>
            </div>
        </div>
        <div className="d-flex justify-content-between align-items-center mx-0 row">
            <div className="col-sm-12 col-md-3">
                <div className="dataTables_length" id="DataTables_Table_0_length">
                    <label>Show 
                        <select name="DataTables_Table_length" aria-controls="DataTables_Table_0" className="form-select" style={{ display: "inline-block"}}>
                        <option value="7">7</option>
                        <option value="10">10</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                        <option value="75">75</option>
                        <option value="100">100</option>
                    </select> entries</label>
                </div>
            </div>
            <div className="col-sm-12 col-md-6">
                <div id="DataTables_Table_0_filter" className="dataTables_filter">
                    <label>Search:
                        <input type="search" className="form-control" placeholder="" aria-controls="DataTables_Table_0" />
                        </label>
                </div>
            </div>
        </div>
    </>
  )
}

export default TableOptions