import React from 'react';
import Table from "./Table"

const Brands = () => {

    return (
        <div className="app-content content ">
            <div className="content-overlay"></div>
            <div className="header-navbar-shadow"></div>
            <div className="header-navbarcontent-wrapper container-xxl p-0">
                <div className="content-header row">
                    <div className="content-body">
                        <Table />
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Brands
