import { useEffect } from 'react'
import {loadShowCustomerAction} from '../../store/actions/CustomerActions'
import {useDispatch,useSelector} from 'react-redux'

export default function Index() {

    const dispatch = useDispatch();
    const customerShow = useSelector(state => state.customerShow.customerShow);
    const location = window.location.href
    var uri = location.split("/");
    console.log(uri.at(-1))
    useEffect(() => {
        dispatch(loadShowCustomerAction(uri.at(-1)));
    }, [dispatch, uri])
    const showData = (key) => {
        return customerShow.data === null ? "No info" : customerShow.data[key]
    }
    console.log(customerShow);
    return (
        
        <section className="app-user-view">
            <div className="row">
                <div className="col-xl-9 col-lg-8 col-md-7">
                    <div className="card user-card">
                        <div className="card-body">
                            <div className="row">
                                    <div className="col-xl-6 col-lg-12 mt-2 mt-xl-0">
                                    <div className="user-info-wrapper">
                                        <div className="d-flex flex-wrap">
                                        <p className="card-text mb-0">
                                        {
                                            customerShow.status_code === 200  ? 
                                                <>
                                                    <div className="d-flex flex-wrap my-50">
                                                        <div className="user-info-title">
                                                            <span className="card-text user-info-title fw-bold mb-0">
                                                                Customer Info :
                                                            </span>
                                                            <span className="card-text user-info-title fw-bold mb-0">
                                                                {showData("firstname") + " " + showData("lastname")}
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div className="d-flex flex-wrap my-50">
                                                        <div className="user-info-title">
                                                            <span className="card-text user-info-title fw-bold mb-0">
                                                                Email :
                                                            </span>
                                                            <span className="card-text user-info-title fw-bold mb-0">
                                                                {showData("email")}
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div className="d-flex flex-wrap my-50">
                                                        <div className="user-info-title">
                                                            <span className="card-text user-info-title fw-bold mb-0">
                                                                Phone :
                                                            </span>
                                                            <span className="card-text user-info-title fw-bold mb-0">
                                                                {showData("phone")}
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div className="d-flex flex-wrap">
                                                        <div className="user-info-title">
                                                            <span className="card-text user-info-title fw-bold mb-0">
                                                                Payment Date :
                                                            </span>
                                                            <span className="card-text user-info-title fw-bold mb-0">
                                                                { showData("create_date")  }
                                                            </span>
                                                        </div>
                                                    </div>
                                                </>
                                                :
                                                customerShow.status_code > 300 ?

                                                    customerShow.message
                                                    :
                                                    <div className="d-flex justify-content-center">
                                                        <div className="spinner-border" role="status">
                                                            <span className="sr-only"></span>
                                                        </div>
                                                    </div>
                                        }
                                        </p>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
    )
}