import React from 'react'
import { Link } from 'react-router-dom'

const Content = () => {
  return (
        <div className="content-right">
            <div className="content-wrapper container-xxl p-0">
                <div className="content-header row">
                </div>
                <div className="content-body"><div className="body-content-overlay"></div>
                    <div className="email-app-list">
                        <div className="app-fixed-search d-flex align-items-center">
                            <div className="sidebar-toggle d-block d-lg-none ms-1">
                            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-menu font-medium-5"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg>
                            </div>
                            <div className="d-flex align-content-center justify-content-between w-100">
                            <div className="input-group input-group-merge">
                                <span className="input-group-text"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-search text-muted"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg></span>
                                <input type="text" className="form-control" id="email-search" placeholder="Search email" aria-label="Search..." aria-describedby="email-search" />
                            </div>
                            </div>
                        </div>
                        <div className="app-action">
                            <div className="action-left">
                                <div className="form-check selectAll">
                                    <input type="checkbox" className="form-check-input" id="selectAllCheck" />
                                    <label className="form-check-label fw-bolder ps-25" for="selectAllCheck">Select All</label>
                                </div>
                            </div>
                            <div className="action-right">
                            <ul className="list-inline m-0">
                                <li className="list-inline-item">
                                <div className="dropdown">
                                    <Link className="dropdown-toggle" id="folder" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-folder font-medium-2"><path d="M22 19a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h5l2 3h9a2 2 0 0 1 2 2z"></path></svg>
                                    </Link>
                                    <div className="dropdown-menu dropdown-menu-end" aria-labelledby="folder">
                                    <Link className="dropdown-item d-flex align-items-center">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-edit-2 font-small-4 me-50"><path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path></svg>
                                        <span>Draft</span>
                                    </Link>
                                    <Link className="dropdown-item d-flex align-items-center">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-info font-small-4 me-50"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="16" x2="12" y2="12"></line><line x1="12" y1="8" x2="12.01" y2="8"></line></svg>
                                        <span>Spam</span>
                                    </Link>
                                    <Link className="dropdown-item d-flex align-items-center">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-trash font-small-4 me-50"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path></svg>
                                        <span>Trash</span>
                                    </Link>
                                    </div>
                                </div>
                                </li>
                                <li className="list-inline-item">
                                    <div className="dropdown">
                                        <Link className="dropdown-toggle" id="tag" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-tag font-medium-2"><path d="M20.59 13.41l-7.17 7.17a2 2 0 0 1-2.83 0L2 12V2h10l8.59 8.59a2 2 0 0 1 0 2.82z"></path><line x1="7" y1="7" x2="7.01" y2="7"></line></svg>
                                        </Link>
                                        <div className="dropdown-menu dropdown-menu-end" aria-labelledby="tag">
                                            <Link className="dropdown-item"><span className="me-50 bullet bullet-success bullet-sm"></span>Personal</Link>
                                            <Link className="dropdown-item"><span className="me-50 bullet bullet-primary bullet-sm"></span>Company</Link>
                                            <Link className="dropdown-item"><span className="me-50 bullet bullet-warning bullet-sm"></span>Important</Link>
                                            <Link className="dropdown-item"><span className="me-50 bullet bullet-danger bullet-sm"></span>Private</Link>
                                        </div>
                                    </div>
                                </li>
                                <li className="list-inline-item mail-unread">
                                    <span className="action-icon"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-mail font-medium-2"><path d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z"></path><polyline points="22,6 12,13 2,6"></polyline></svg></span>
                                </li>
                                <li className="list-inline-item mail-delete">
                                    <span className="action-icon"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-trash-2 font-medium-2"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg></span>
                                </li>
                            </ul>
                            </div>
                        </div>
                        <div className="email-user-list ps ps--active-y">
                            <ul className="email-media-list">
                            <li className="d-flex user-mail mail-read">
                                <div className="mail-left pe-50">
                                <div className="avatar">
                                    <img src="../../../app-assets/images/portrait/small/avatar-s-20.jpg" alt="avatarholder" />
                                </div>
                                <div className="user-action">
                                    <div className="form-check">
                                    <input type="checkbox" className="form-check-input" id="customCheck1" />
                                    <label className="form-check-label" for="customCheck1"></label>
                                    </div>
                                    <span className="email-favorite"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-star"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg></span>
                                </div>
                                </div>
                                <div className="mail-body">
                                <div className="mail-details">
                                    <div className="mail-items">
                                    <h5 className="mb-25">Tonny Deep</h5>
                                    <span className="text-truncate">🎯 Focused impactful open system </span>
                                    </div>
                                    <div className="mail-meta-item">
                                    <span className="me-50 bullet bullet-success bullet-sm"></span>
                                    <span className="mail-date">4:14 AM</span>
                                    </div>
                                </div>
                                <div className="mail-message">
                                    <p className="text-truncate mb-0">
                                    Hey John, bah kivu decrete epanorthotic unnotched Argyroneta nonius veratrine preimaginary saunders
                                    demidolmen Chaldaic allusiveness lorriker unworshipping ribaldish tableman hendiadys outwrest unendeavored
                                    fulfillment scientifical Pianokoto CheloniaFreudian sperate unchary hyperneurotic phlogiston duodecahedron
                                    unflown Paguridea catena disrelishable Stygian paleopsychology cantoris phosphoritic disconcord fruited
                                    inblow somewhatly ilioperoneal forrard palfrey Satyrinae outfreeman melebiose
                                    </p>
                                </div>
                                </div>
                            </li>
                            <li className="d-flex user-mail">
                                <div className="mail-left pe-50">
                                <div className="avatar">
                                    <img src="../../../app-assets/images/portrait/small/avatar-s-17.jpg" alt="Generic placeholder" />
                                </div>
                                <div className="user-action">
                                    <div className="form-check">
                                    <input type="checkbox" className="form-check-input" id="customCheck2" />
                                    <label className="form-check-label" for="customCheck2"></label>
                                    </div>
                                    <span className="email-favorite"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-star"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg></span>
                                </div>
                                </div>
                                <div className="mail-body">
                                <div className="mail-details">
                                    <div className="mail-items">
                                    <h5 className="mb-25">Louis Welch</h5>
                                    <span className="text-truncate">Thanks, Let's do it.🤩</span>
                                    </div>
                                    <div className="mail-meta-item">
                                    <span className="me-50 bullet bullet-danger bullet-sm"></span>
                                    <span className="mail-date">2:15 AM</span>
                                    </div>
                                </div>
                                <div className="mail-message">
                                    <p className="mb-0 text-truncate">
                                    Hi, Can we connect today? Cheesecake croissant jelly beans. Cake caramels pie chocolate. Muffin jujubes
                                    dragée carrot cake candy icing bonbon. Danish caramels topping oat cake sweet roll liquorice tootsie roll
                                    halvah.Chocolate bar jujubes jelly-o tart tiramisu croissant dragée cupcake jelly.
                                    </p>
                                </div>
                                </div>
                            </li>
                            <li className="d-flex user-mail mail-read">
                                <div className="mail-left pe-50">
                                <div className="avatar">
                                    <img src="../../../app-assets/images/portrait/small/avatar-s-7.jpg" alt="Generic placeholder" />
                                </div>
                                <div className="user-action">
                                    <div className="form-check">
                                    <input type="checkbox" className="form-check-input" id="customCheck3" />
                                    <label className="form-check-label" for="customCheck3"></label>
                                    </div>
                                    <span className="email-favorite"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-star"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg></span>
                                </div>
                                </div>
                                <div className="mail-body">
                                <div className="mail-details">
                                    <div className="mail-items">
                                    <h5 className="mb-25">Envato Market</h5>
                                    <span className="text-truncate">👋 You have new comment...</span>
                                    </div>
                                    <div className="mail-meta-item">
                                    <span className="me-50 bullet bullet-success bullet-sm"></span>
                                    <span className="mail-date">2:15 AM</span>
                                    </div>
                                </div>
                                <div className="mail-message">
                                    <p className="mb-0 text-truncate">
                                    Hi There, Cotton candy jujubes ice cream candy. Oat cake jelly jelly brownie danish marzipan gummi bears.
                                    Cupcake sweet bonbon tart. Sweet croissant jelly beans dragée chocolate cake gingerbread topping chocolate
                                    bar lemon drops.
                                    </p>
                                </div>
                                </div>
                            </li>
                            <li className="d-flex user-mail mail-read">
                                <div className="mail-left pe-50">
                                <div className="avatar">
                                    <img src="../../../app-assets/images/portrait/small/avatar-s-5.jpg" alt="Generic placeholder" />
                                </div>
                                <div className="user-action">
                                    <div className="form-check">
                                    <input type="checkbox" className="form-check-input" id="customCheck4" />
                                    <label className="form-check-label" for="customCheck4"></label>
                                    </div>
                                    <span className="email-favorite"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-star"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg></span>
                                </div>
                                </div>
                                <div className="mail-body">
                                <div className="mail-details">
                                    <div className="mail-items">
                                    <h5 className="mb-25">Sarah Montgomery</h5>
                                    <span className="text-truncate">Your New UI.</span>
                                    </div>
                                    <div className="mail-meta-item">
                                    <span className="me-50 bullet bullet-warning bullet-sm"></span>
                                    <span className="mail-date">Yesterday</span>
                                    </div>
                                </div>
                                <div className="mail-message">
                                    <p className="text-truncate mb-0">
                                    Hello, Everything looks good. Pastry marshmallow sugar plum. Gingerbread bonbon fruitcake gummi bears
                                    wafer chocolate cake gummies tart ice cream. Danish topping biscuit dessert donut dessert. Chocolate
                                    jelly-o topping marzipan fruitcake.
                                    </p>
                                </div>
                                </div>
                            </li>
                            <li className="d-flex user-mail">
                                <div className="mail-left pe-50">
                                <div className="avatar">
                                    <img src="../../../app-assets/images/portrait/small/avatar-s-3.jpg" alt="Generic placeholder" />
                                </div>
                                <div className="user-action">
                                    <div className="form-check">
                                    <input type="checkbox" className="form-check-input" id="customCheck5" />
                                    <label className="form-check-label" for="customCheck5"></label>
                                    </div>
                                    <span className="email-favorite"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-star"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg></span>
                                </div>
                                </div>
                                <div className="mail-body">
                                <div className="mail-details">
                                    <div className="mail-items">
                                    <h5 className="mb-25">Ardis Balderson</h5>
                                    <span className="text-truncate mb-0">Focused impactful open system</span>
                                    </div>
                                    <div className="mail-meta-item">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-paperclip"><path d="M21.44 11.05l-9.19 9.19a6 6 0 0 1-8.49-8.49l9.19-9.19a4 4 0 0 1 5.66 5.66l-9.2 9.19a2 2 0 0 1-2.83-2.83l8.49-8.48"></path></svg>
                                    <span className="mx-50 bullet bullet-warning bullet-sm"></span>
                                    <span className="mail-date">15 July</span>
                                    </div>
                                </div>
                                <div className="mail-message">
                                    <p className="mb-0 text-truncate">
                                    Hey John, bah kivu decrete epanorthotic unnotched Argyroneta nonius veratrine preimaginary saunders
                                    demidolmen Chaldaic allusiveness lorriker unworshipping ribaldish tableman hendiadys outwrest unendeavored
                                    fulfillment scientifical Pianokoto CheloniaFreudian sperate unchary hyperneurotic phlogiston duodecahedron
                                    unflown Paguridea catena disrelishable Stygian paleopsychology cantoris phosphoritic disconcord fruited
                                    inblow somewhatly ilioperoneal forrard palfrey
                                    </p>
                                </div>
                                </div>
                            </li>
                            <li className="d-flex user-mail">
                                <div className="mail-left pe-50">
                                <div className="avatar">
                                    <img src="../../../app-assets/images/portrait/small/avatar-s-8.jpg" alt="Generic placeholder" />
                                </div>
                                <div className="user-action">
                                    <div className="form-check">
                                    <input type="checkbox" className="form-check-input" id="customCheck6" />
                                    <label className="form-check-label" for="customCheck6"></label>
                                    </div>
                                    <span className="email-favorite"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-star"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg></span>
                                </div>
                                </div>
                                <div className="mail-body">
                                <div className="mail-details">
                                    <div className="mail-items">
                                    <h5 className="mb-25">Modestine Spat</h5>
                                    <span className="text-truncate mb-0">Profound systemic alliance 🎉</span>
                                    </div>
                                    <div className="mail-meta-item">
                                    <span className="mx-50 bullet bullet-primary bullet-sm"></span>
                                    <span className="mail-date">11 July</span>
                                    </div>
                                </div>
                                <div className="mail-message">
                                    <p className="mb-0 text-truncate">
                                    Hey John, Parthenopean logeion chipwood tonsilitic cockleshell substance Stilbum dismayed tape Alderamin
                                    Phororhacos bridewain zoonomia lujaurite printline extraction weanedness charterless splitmouth bindoree
                                    unfit philological Pythonissa scintillescentcinchonism sabbaton thyrocricoid dissuasively schematograph
                                    immerse pristane stimulability unreligion uncomplemental uteritis nef bavenite Hachiman teleutosorus
                                    anterolateral infirmate Nahani Hyla barile farthing
                                    </p>
                                </div>
                                </div>
                            </li>
                            <li className="d-flex user-mail mail-read">
                                <div className="mail-left pe-50">
                                <div className="avatar">
                                    <img src="../../../app-assets/images/portrait/small/avatar-s-11.jpg" alt="Generic placeholder" />
                                </div>
                                <div className="user-action">
                                    <div className="form-check">
                                    <input type="checkbox" className="form-check-input" id="customCheck7" />
                                    <label className="form-check-label" for="customCheck7"></label>
                                    </div>
                                    <span className="email-favorite"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-star"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg></span>
                                </div>
                                </div>
                                <div className="mail-body">
                                <div className="mail-details">
                                    <div className="mail-items">
                                    <h5 className="mb-25">Eb Begg</h5>
                                    <span className="text-truncate mb-0">Organized value-added model</span>
                                    </div>
                                    <div className="mail-meta-item">
                                    <span className="mx-50 bullet bullet-success bullet-sm"></span>
                                    <span className="mail-date">1 July</span>
                                    </div>
                                </div>
                                <div className="mail-message">
                                    <p className="mb-0 text-truncate">
                                    Hello Sir, Lituola restrengthen bathofloric manciple decaffeinize Debby aciliated eatage proscribe
                                    prejurisdiction buttle quacky hyposecretion indemonstrableness schelling lymphopathy consumptivity
                                    nonappointment filminess spumiform erotogenicity equestrianize boneflower interlardationallocate ponzite
                                    cote guilder tuff strind blamefully cocaine monstrously apocalyptically sublanate cherubimical
                                    oligoplasmia Miltonian hydrazyl unbeset statured Unami Cordeau strouthiocamelian geitjie
                                    </p>
                                </div>
                                </div>
                            </li>
                            <li className="d-flex user-mail mail-read">
                                <div className="mail-left pe-50">
                                <div className="avatar">
                                    <img src="../../../app-assets/images/portrait/small/avatar-s-10.jpg" alt="Generic placeholder" />
                                </div>
                                <div className="user-action">
                                    <div className="form-check">
                                    <input type="checkbox" className="form-check-input" id="customCheck8" />
                                    <label className="form-check-label" for="customCheck8"></label>
                                    </div>
                                    <span className="email-favorite"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-star"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg></span>
                                </div>
                                </div>
                                <div className="mail-body">
                                <div className="mail-details">
                                    <div className="mail-items">
                                    <h5 className="mb-25">Waldemar Mannering</h5>
                                    <span className="text-truncate mb-0">Quality-focused methodical flexibility</span>
                                    </div>
                                    <div className="mail-meta-item">
                                    <span className="mx-50 bullet bullet-danger bullet-sm"></span>
                                    <span className="mail-date">19 Jun</span>
                                    </div>
                                </div>
                                <div className="mail-message">
                                    <p className="mb-0 text-truncate">
                                    Hi John, wartproof ketoheptose incomplicate hyomental organal supermaterial monogene sophister nizamate
                                    rightle multifilament phloroglucic overvehement boatloading derelictly probudgeting archantiquary
                                    unknighted pallograph Volcanalia Jacobitiana ethyl neth Jugataenoumenalize irredential energeia
                                    phlebotomist galp dactylitis unparticipated solepiece demure metarhyolite toboggan unpleased perilaryngeal
                                    binoxalate rabbitry atomic duali dihexahedron Pseudogryphus boomboat obelisk
                                    </p>
                                </div>
                                </div>
                            </li>
                            <li className="d-flex user-mail mail-read">
                                <div className="mail-left pe-50">
                                <div className="avatar">
                                    <img src="../../../app-assets/images/portrait/small/avatar-s-6.jpg" alt="Generic placeholder" />
                                </div>
                                <div className="user-action">
                                    <div className="form-check">
                                    <input type="checkbox" className="form-check-input" id="customCheck9" />
                                    <label className="form-check-label" for="customCheck9"></label>
                                    </div>
                                    <span className="email-favorite"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-star"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg></span>
                                </div>
                                </div>
                                <div className="mail-body">
                                <div className="mail-details">
                                    <div className="mail-items">
                                    <h5 className="mb-25">Louetta Esses</h5>
                                    <span className="text-truncate mb-0">Company Report</span>
                                    </div>
                                    <div className="mail-meta-item">
                                    <span className="mx-50 bullet bullet-primary bullet-sm"></span>
                                    <span className="mail-date">2 Jun</span>
                                    </div>
                                </div>
                                <div className="mail-message">
                                    <p className="mb-0 text-truncate">
                                    Hi John,Biscuit lemon drops marshmallow. Cotton candy marshmallow bear claw. Dragée tiramisu cookie cotton
                                    candy. Carrot cake sweet roll I love macaroon wafer jelly soufflé I love dragée. Jujubes jelly I love
                                    carrot cake topping I love. Sweet candy I love
                                    </p>
                                </div>
                                </div>
                            </li>
                            <li className="d-flex user-mail">
                                <div className="mail-left pe-50">
                                <div className="avatar">
                                    <img src="../../../app-assets/images/portrait/small/avatar-s-9.jpg" alt="Generic placeholder" />
                                </div>
                                <div className="user-action">
                                    <div className="form-check">
                                    <input type="checkbox" className="form-check-input" id="customCheck10" />
                                    <label className="form-check-label" for="customCheck10"></label>
                                    </div>
                                    <span className="email-favorite"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-star"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg></span>
                                </div>
                                </div>
                                <div className="mail-body">
                                <div className="mail-details">
                                    <div className="mail-items">
                                    <h5 className="mb-25">Tressa Gass</h5>
                                    <span className="text-truncate mb-0">Theme Update</span>
                                    </div>
                                    <div className="mail-meta-item">
                                    <span className="mx-50 bullet bullet-primary bullet-sm"></span>
                                    <span className="mail-date">29 May</span>
                                    </div>
                                </div>
                                <div className="mail-message">
                                    <p className="mb-0 text-truncate">
                                    Hello John,Chocolate bar chupa chups sweet roll chocolate muffin macaroon liquorice tart. Carrot cake
                                    topping jelly-o cupcake sweet apple pie jelly I love. Chocolate cake I love dessert carrot cake tootsie
                                    roll chocolate I love. Tootsie roll pie marzipan sesame snaps.
                                    </p>
                                </div>
                                </div>
                            </li>
                            <li className="d-flex user-mail">
                                <div className="mail-left pe-50">
                                <div className="avatar">
                                    <img src="../../../app-assets/images/portrait/small/avatar-s-20.jpg" alt="Generic placeholder" />
                                </div>
                                <div className="user-action">
                                    <div className="form-check">
                                    <input type="checkbox" className="form-check-input" id="customCheck11" />
                                    <label className="form-check-label" for="customCheck11"></label>
                                    </div>
                                    <span className="email-favorite"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-star"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg></span>
                                </div>
                                </div>
                                <div className="mail-body">
                                <div className="mail-details">
                                    <div className="mail-items">
                                    <h5 className="mb-25">Tommy Sicilia</h5>
                                    <span className="text-truncate mb-0">Thanks, Let's do it.</span>
                                    </div>
                                    <div className="mail-meta-item">
                                    <span className="mx-50 bullet bullet-warning bullet-sm"></span>
                                    <span className="mail-date">17 May</span>
                                    </div>
                                </div>
                                <div className="mail-message">
                                    <p className="mb-0 text-truncate">
                                    Hi John,Biscuit lemon drops marshmallow. Cotton candy marshmallow bear claw. Dragée tiramisu cookie cotton
                                    candy. Carrot cake sweet roll I love macaroon wafer jelly soufflé I love dragée. Jujubes jelly I love
                                    carrot cake topping I love. Sweet candy I love.
                                    </p>
                                </div>
                                </div>
                            </li>
                            <li className="d-flex user-mail mail-read">
                                <div className="mail-left pe-50">
                                <div className="avatar">
                                    <img src="../../../app-assets/images/portrait/small/avatar-s-17.jpg" alt="Generic placeholder" />
                                </div>
                                <div className="user-action">
                                    <div className="form-check">
                                    <input type="checkbox" className="form-check-input" id="customCheck12" />
                                    <label className="form-check-label" for="customCheck12"></label>
                                    </div>
                                    <span className="email-favorite"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-star"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg></span>
                                </div>
                                </div>
                                <div className="mail-body">
                                <div className="mail-details">
                                    <div className="mail-items">
                                    <h5 className="mb-25">Heather Howell</h5>
                                    <span className="text-truncate mb-0">Thanks, Let's do it.</span>
                                    </div>
                                    <div className="mail-meta-item">
                                    <span className="mx-50 bullet bullet-warning bullet-sm"></span>
                                    <span className="mail-date">21 Mar</span>
                                    </div>
                                </div>
                                <div className="mail-message">
                                    <p className="mb-0 text-truncate">
                                    Hi John,Biscuit lemon drops marshmallow. Marzipan carrot cake soufflé. Toffee tiramisu pudding cotton
                                    candy powder jujubes pie. Topping danish sweet croissant liquorice lemon drops cake oat cake brownie.
                                    Cupcake liquorice tart tootsie roll sugar plum chocolate bar oat cake sweet roll.
                                    </p>
                                </div>
                                </div>
                            </li>
                            </ul>
                            <div className="no-results">
                            <h5>No Items Found</h5>
                            </div>
                        <div className="ps__rail-x" style={{ left: "0px", bottom: "0px;" }}><div className="ps__thumb-x" tabindex="0" style={{ left: "0px",width: "0px" }}></div></div><div className="ps__rail-y" style={{ top: "0px", height: "192px", right: "0px;" }}><div className="ps__thumb-y" tabindex="0" style={{ top: "0px", height: "25px;" }}></div></div></div>
                        </div>
                        <div className="email-app-details">
                        <div className="email-detail-header">
                            <div className="email-header-left d-flex align-items-center">
                            <span className="go-back me-1"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-chevron-left font-medium-4"><polyline points="15 18 9 12 15 6"></polyline></svg></span>
                            <h4 className="email-subject mb-0">Focused open system 😃</h4>
                            </div>
                            <div className="email-header-right ms-2 ps-1">
                            <ul className="list-inline m-0">
                                <li className="list-inline-item">
                                <span className="action-icon favorite"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-star font-medium-2"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg></span>
                                </li>
                                <li className="list-inline-item">
                                <div className="dropdown no-arrow">
                                    <Link className="dropdown-toggle" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-folder font-medium-2"><path d="M22 19a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h5l2 3h9a2 2 0 0 1 2 2z"></path></svg>
                                    </Link>
                                    <div className="dropdown-menu" aria-labelledby="folder">
                                    <Link className="dropdown-item d-flex align-items-center">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-edit-2 font-medium-3 me-50"><path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path></svg>
                                        <span>Draft</span>
                                    </Link>
                                    <Link className="dropdown-item d-flex align-items-center">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-info font-medium-3 me-50"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="16" x2="12" y2="12"></line><line x1="12" y1="8" x2="12.01" y2="8"></line></svg>
                                        <span>Spam</span>
                                    </Link>
                                    <Link className="dropdown-item d-flex align-items-center">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-trash font-medium-3 me-50"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path></svg>
                                        <span>Trash</span>
                                    </Link>
                                    </div>
                                </div>
                                </li>
                                <li className="list-inline-item">
                                <div className="dropdown no-arrow">
                                    <Link className="dropdown-toggle" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-tag font-medium-2"><path d="M20.59 13.41l-7.17 7.17a2 2 0 0 1-2.83 0L2 12V2h10l8.59 8.59a2 2 0 0 1 0 2.82z"></path><line x1="7" y1="7" x2="7.01" y2="7"></line></svg>
                                    </Link>
                                    <div className="dropdown-menu" aria-labelledby="tag">
                                    <Link className="dropdown-item"><span className="me-50 bullet bullet-success bullet-sm"></span>Personal</Link>
                                    <Link className="dropdown-item"><span className="me-50 bullet bullet-primary bullet-sm"></span>Company</Link>
                                    <Link className="dropdown-item"><span className="me-50 bullet bullet-warning bullet-sm"></span>Important</Link>
                                    <Link className="dropdown-item"><span className="me-50 bullet bullet-danger bullet-sm"></span>Private</Link>
                                    </div>
                                </div>
                                </li>
                                <li className="list-inline-item">
                                <span className="action-icon"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-mail font-medium-2"><path d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z"></path><polyline points="22,6 12,13 2,6"></polyline></svg></span>
                                </li>
                                <li className="list-inline-item">
                                <span className="action-icon"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-trash font-medium-2"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path></svg></span>
                                </li>
                                <li className="list-inline-item email-prev">
                                <span className="action-icon"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-chevron-left font-medium-2"><polyline points="15 18 9 12 15 6"></polyline></svg></span>
                                </li>
                                <li className="list-inline-item email-next">
                                <span className="action-icon"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-chevron-right font-medium-2"><polyline points="9 18 15 12 9 6"></polyline></svg></span>
                                </li>
                            </ul>
                            </div>
                        </div>
                        <div className="email-scroll-area ps ps--active-y">
                            <div className="row">
                            <div className="col-12">
                                <div className="email-label">
                                <span className="mail-label badge rounded-pill badge-light-primary">Company</span>
                                </div>
                            </div>
                            </div>
                            <div className="row">
                            <div className="col-12">
                                <div className="card">
                                <div className="card-header email-detail-head">
                                    <div className="user-details d-flex justify-content-between align-items-center flex-wrap">
                                    <div className="avatar me-75">
                                        <img src="../../../app-assets/images/portrait/small/avatar-s-9.jpg" alt="avatarholder" width="48" height="48" />
                                    </div>
                                    <div className="mail-items">
                                        <h5 className="mb-0">Carlos Williamson</h5>
                                        <div className="email-info-dropup dropdown">
                                        <span role="button" className="dropdown-toggle font-small-3 text-muted" id="card_top01" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            carlos@gmail.com
                                        </span>
                                        <div className="dropdown-menu" aria-labelledby="card_top01">
                                            <table className="table table-sm table-borderless">
                                            <tbody>
                                                <tr>
                                                <td className="text-end">From:</td>
                                                <td>carlos@gmail.com</td>
                                                </tr>
                                                <tr>
                                                <td className="text-end">To:</td>
                                                <td>johndoe@ow.ly</td>
                                                </tr>
                                                <tr>
                                                <td className="text-end">Date:</td>
                                                <td>14:58, 29 Aug 2020</td>
                                                </tr>
                                            </tbody>
                                            </table>
                                        </div>
                                        </div>
                                    </div>
                                    </div>
                                    <div className="mail-meta-item d-flex align-items-center">
                                    <small className="mail-date-time text-muted">29 Aug, 2020, 14:58</small>
                                    <div className="dropdown ms-50">
                                        <div role="button" className="dropdown-toggle hide-arrow" id="email_more" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-more-vertical font-medium-2"><circle cx="12" cy="12" r="1"></circle><circle cx="12" cy="5" r="1"></circle><circle cx="12" cy="19" r="1"></circle></svg>
                                        </div>
                                        <div className="dropdown-menu dropdown-menu-end" aria-labelledby="email_more">
                                        <div className="dropdown-item"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-corner-up-left me-50"><polyline points="9 14 4 9 9 4"></polyline><path d="M20 20v-7a4 4 0 0 0-4-4H4"></path></svg>Reply</div>
                                        <div className="dropdown-item"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-corner-up-right me-50"><polyline points="15 14 20 9 15 4"></polyline><path d="M4 20v-7a4 4 0 0 1 4-4h12"></path></svg>Forward</div>
                                        <div className="dropdown-item"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-trash-2 me-50"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>Delete</div>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                <div className="card-body mail-message-wrapper pt-2">
                                    <div className="mail-message">
                                    <p className="card-text">Hey John,</p>
                                    <p className="card-text">
                                        bah kivu decrete epanorthotic unnotched Argyroneta nonius veratrine preimaginary saunders demidolmen
                                        Chaldaic allusiveness lorriker unworshipping ribaldish tableman hendiadys outwrest unendeavored
                                        fulfillment scientifical Pianokoto Chelonia
                                    </p>
                                    </div>
                                </div>
                                </div>
                            </div>
                            </div>
                            <div className="row">
                            <div className="col-12">
                                <div className="card">
                                <div className="card-header email-detail-head">
                                    <div className="user-details d-flex justify-content-between align-items-center flex-wrap">
                                    <div className="avatar me-75">
                                        <img src="../../../app-assets/images/portrait/small/avatar-s-18.jpg" alt="avatarholder" width="48" height="48" />
                                    </div>
                                    <div className="mail-items">
                                        <h5 className="mb-0">Ardis Balderson</h5>
                                        <div className="email-info-dropup dropdown">
                                        <span role="button" className="dropdown-toggle font-small-3 text-muted" id="dropdownMenuButton200" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            abaldersong@utexas.edu
                                        </span>
                                        <div className="dropdown-menu" aria-labelledby="dropdownMenuButton200">
                                            <table className="table table-sm table-borderless">
                                            <tbody>
                                                <tr>
                                                <td className="text-end">From:</td>
                                                <td>abaldersong@utexas.edu</td>
                                                </tr>
                                                <tr>
                                                <td className="text-end">To:</td>
                                                <td>johndoe@ow.ly</td>
                                                </tr>
                                                <tr>
                                                <td className="text-end">Date:</td>
                                                <td>4:25 AM 13 Jan 2018</td>
                                                </tr>
                                            </tbody>
                                            </table>
                                        </div>
                                        </div>
                                    </div>
                                    </div>
                                    <div className="mail-meta-item d-flex align-items-center">
                                    <small className="mail-date-time text-muted">17 May, 2020, 4:14</small>
                                    <div className="dropdown ms-50">
                                        <div role="button" className="dropdown-toggle hide-arrow" id="email_more_2" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-more-vertical font-medium-2"><circle cx="12" cy="12" r="1"></circle><circle cx="12" cy="5" r="1"></circle><circle cx="12" cy="19" r="1"></circle></svg>
                                        </div>
                                        <div className="dropdown-menu dropdown-menu-end" aria-labelledby="email_more_2">
                                        <div className="dropdown-item"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-corner-up-left me-50"><polyline points="9 14 4 9 9 4"></polyline><path d="M20 20v-7a4 4 0 0 0-4-4H4"></path></svg>Reply</div>
                                        <div className="dropdown-item"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-corner-up-right me-50"><polyline points="15 14 20 9 15 4"></polyline><path d="M4 20v-7a4 4 0 0 1 4-4h12"></path></svg>Forward</div>
                                        <div className="dropdown-item"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-trash-2 me-50"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>Delete</div>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                <div className="card-body mail-message-wrapper pt-2">
                                    <div className="mail-message">
                                    <p className="card-text">Hey John,</p>
                                    <p className="card-text">
                                        bah kivu decrete epanorthotic unnotched Argyroneta nonius veratrine preimaginary saunders demidolmen
                                        Chaldaic allusiveness lorriker unworshipping ribaldish tableman hendiadys outwrest unendeavored
                                        fulfillment scientifical Pianokoto Chelonia
                                    </p>
                                    <p className="card-text">
                                        Freudian sperate unchary hyperneurotic phlogiston duodecahedron unflown Paguridea catena disrelishable
                                        Stygian paleopsychology cantoris phosphoritic disconcord fruited inblow somewhatly ilioperoneal forrard
                                        palfrey Satyrinae outfreeman melebiose
                                    </p>
                                    </div>
                                </div>
                                <div className="card-footer">
                                    <div className="mail-attachments">
                                    <div className="d-flex align-items-center mb-1">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-paperclip font-medium-1 me-50"><path d="M21.44 11.05l-9.19 9.19a6 6 0 0 1-8.49-8.49l9.19-9.19a4 4 0 0 1 5.66 5.66l-9.2 9.19a2 2 0 0 1-2.83-2.83l8.49-8.48"></path></svg>
                                        <h5 className="fw-bolder text-body mb-0">2 Attachments</h5>
                                    </div>
                                    <div className="d-flex flex-column">
                                        <Link className="mb-50">
                                        <img src="../../../app-assets/images/icons/doc.png" className="me-25" alt="png" height="18" />
                                        <small className="text-muted fw-bolder">interdum.docx</small>
                                        </Link>
                                        <Link>
                                        <img src="../../../app-assets/images/icons/jpg.png" className="me-25" alt="png" height="18" />
                                        <small className="text-muted fw-bolder">image.png</small>
                                        </Link>
                                    </div>
                                    </div>
                                </div>
                                </div>
                            </div>
                            </div>
                            <div className="row">
                            <div className="col-12">
                                <div className="card">
                                <div className="card-body">
                                    <div className="d-flex justify-content-between">
                                    <h5 className="mb-0">
                                        Click here to
                                        <Link>Reply</Link>
                                        or
                                        <Link>Forward</Link>
                                    </h5>
                                    </div>
                                </div>
                                </div>
                            </div>
                            </div>
                        <div className="ps__rail-x" style={{ left: "0px", bottom: "0px;" }}><div className="ps__thumb-x" tabindex="0" style={{ left: "0px", width: "0px;" }}></div></div><div className="ps__rail-y" style={{ top: "0px", height: "217px", right: "0px;" }}><div className="ps__thumb-y" tabindex="0" style={{ top: "0px", height: "53px;" }}></div></div></div>
                        </div>
                        <div className="modal modal-sticky" id="compose-mail" data-bs-keyboard="false">
                        <div className="modal-dialog modal-lg modal-dialog-scrollable">
                            <div className="modal-content p-0">
                            <div className="modal-header">
                                <h5 className="modal-title">Compose Mail</h5>
                                <div className="modal-actions">
                                <Link className="text-body me-75"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-minus"><line x1="5" y1="12" x2="19" y2="12"></line></svg></Link>
                                <Link className="text-body me-75 compose-maximize"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-maximize-2"><polyline points="15 3 21 3 21 9"></polyline><polyline points="9 21 3 21 3 15"></polyline><line x1="21" y1="3" x2="14" y2="10"></line><line x1="3" y1="21" x2="10" y2="14"></line></svg></Link>
                                <Link className="text-body" data-bs-dismiss="modal" aria-label="Close"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-x"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg></Link>
                                </div>
                            </div>
                            <div className="modal-body flex-grow-1 p-0">
                                <form className="compose-form">
                                <div className="compose-mail-form-field select2-primary">
                                    <label for="email-to" className="form-label">To: </label>
                                    <div className="flex-grow-1">
                                    <div className="position-relative"><select className="select2 form-select w-100 select2-hidden-accessible" id="email-to" multiple="" data-select2-id="email-to" tabindex="-1" aria-hidden="true">
                                        <option data-avatar="1-small.png" value="Jane Foster">Jane Foster</option>
                                        <option data-avatar="3-small.png" value="Donna Frank">Donna Frank</option>
                                        <option data-avatar="5-small.png" value="Gabrielle Robertson">Gabrielle Robertson</option>
                                        <option data-avatar="7-small.png" value="Lori Spears">Lori Spears</option>
                                    </select><span className="select2 select2-container select2-container--default" dir="ltr" data-select2-id="1" style={{ width: "auto" }}><span className="selection"><span className="select2-selection select2-selection--multiple" aria-haspopup="true" aria-expanded="false" tabindex="-1" aria-disabled="false"><ul className="select2-selection__rendered"><li className="select2-search select2-search--inline">
                                        <input className="select2-search__field" type="search" tabindex="0" autocomplete="off" autocorrect="off" autocapitalize="none" spellcheck="false" role="searchbox" aria-autocomplete="list" placeholder="" style={{ width: "0.75em" }} /></li></ul></span></span><span className="dropdown-wrapper" aria-hidden="true"></span></span></div>
                                    </div>
                                    <div>
                                    <Link className="toggle-cc text-body me-1">Cc</Link>
                                    <Link className="toggle-bcc text-body">Bcc</Link>
                                    </div>
                                </div>
                                <div className="compose-mail-form-field cc-wrapper" style={{ display: "none" }}>
                                    <label for="emailCC" className="form-label">Cc: </label>
                                    <div className="flex-grow-1">
                                    <input type="text" id="emailCC" className="form-control" placeholder="CC"/>
                                    <div className="position-relative"><select className="select2 form-select w-100 select2-hidden-accessible" id="emailCC" multiple="" data-select2-id="emailCC" tabindex="-1" aria-hidden="true">
                                        <option data-avatar="1-small.png" value="Jane Foster">Jane Foster</option>
                                        <option data-avatar="3-small.png" value="Donna Frank">Donna Frank</option>
                                        <option data-avatar="5-small.png" value="Gabrielle Robertson">Gabrielle Robertson</option>
                                        <option data-avatar="7-small.png" value="Lori Spears">Lori Spears</option>
                                    </select><span className="select2 select2-container select2-container--default" dir="ltr" data-select2-id="2" style={{ width: "auto" }}><span className="selection"><span className="select2-selection select2-selection--multiple" aria-haspopup="true" aria-expanded="false" tabindex="-1" aria-disabled="false"><ul className="select2-selection__rendered"><li className="select2-search select2-search--inline">
                                        <input className="select2-search__field" type="search" tabindex="0" autocomplete="off" autocorrect="off" autocapitalize="none" spellcheck="false" role="searchbox" aria-autocomplete="list" placeholder="" style={{ width: "0.75em" }} /></li></ul></span></span><span className="dropdown-wrapper" aria-hidden="true"></span></span></div>
                                    </div>
                                    <Link className="text-body toggle-cc"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-x"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg></Link>
                                </div>
                                <div className="compose-mail-form-field bcc-wrapper" style={{ display: "none" }}>
                                    <label for="emailBCC" className="form-label">Bcc: </label>
                                    <div className="flex-grow-1">
                                    <input type="text" id="emailBCC" className="form-control" placeholder="BCC"/>
                                    <div className="position-relative"><select className="select2 form-select w-100 select2-hidden-accessible" id="emailBCC" multiple="" data-select2-id="emailBCC" tabindex="-1" aria-hidden="true">
                                        <option data-avatar="1-small.png" value="Jane Foster">Jane Foster</option>
                                        <option data-avatar="3-small.png" value="Donna Frank">Donna Frank</option>
                                        <option data-avatar="5-small.png" value="Gabrielle Robertson">Gabrielle Robertson</option>
                                        <option data-avatar="7-small.png" value="Lori Spears">Lori Spears</option>
                                    </select><span className="select2 select2-container select2-container--default" dir="ltr" data-select2-id="3" style={{ width: "auto" }}><span className="selection"><span className="select2-selection select2-selection--multiple" aria-haspopup="true" aria-expanded="false" tabindex="-1" aria-disabled="false"><ul className="select2-selection__rendered"><li className="select2-search select2-search--inline"><input className="select2-search__field" type="search" tabindex="0" autocomplete="off" autocorrect="off" autocapitalize="none" spellcheck="false" role="searchbox" aria-autocomplete="list" placeholder="" style={{ width: "0.75em;" }} /></li></ul></span></span><span className="dropdown-wrapper" aria-hidden="true"></span></span></div>
                                    </div>
                                    <Link className="text-body toggle-bcc"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-x"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg></Link>
                                </div>
                                <div className="compose-mail-form-field">
                                    <label for="emailSubject" className="form-label">Subject: </label>
                                    <input type="text" id="emailSubject" className="form-control" placeholder="Subject" name="emailSubject" />
                                </div>
                                <div id="message-editor">
                                    <div className="editor ql-container ql-snow" data-placeholder="Type message..."><div className="ql-editor ql-blank" data-gramm="false" contenteditable="true" data-placeholder="Message"><p><br /></p></div><div className="ql-clipboard" contenteditable="true" tabindex="-1"></div><div className="ql-tooltip ql-hidden"><Link className="ql-preview" rel="noopener noreferrer" target="_blank" href="about:blank"></Link><input type="text" data-formula="e=mc^2" data-link="https://quilljs.com" data-video="Embed URL" /><Link className="ql-action"></Link><Link className="ql-remove"></Link></div></div>
                                    <div className="compose-editor-toolbar ql-toolbar ql-snow">
                                    <span className="ql-formats me-0">
                                        <span className="ql-font ql-picker"><span className="ql-picker-label" tabindex="0" role="button" aria-expanded="false" aria-controls="ql-picker-options-0" data-label="Sailec Light"><svg viewBox="0 0 18 18"> <polygon className="ql-stroke" points="7 11 9 13 11 11 7 11"></polygon> <polygon className="ql-stroke" points="7 7 9 5 11 7 7 7"></polygon> </svg></span><span className="ql-picker-options" aria-hidden="true" tabindex="-1" id="ql-picker-options-0"><span tabindex="0" role="button" className="ql-picker-item ql-selected" data-label="Sailec Light"></span><span tabindex="0" role="button" className="ql-picker-item" data-value="sofia" data-label="Sofia Pro"></span><span tabindex="0" role="button" className="ql-picker-item" data-value="slabo" data-label="Slabo 27px"></span><span tabindex="0" role="button" className="ql-picker-item" data-value="roboto" data-label="Roboto Slab"></span><span tabindex="0" role="button" className="ql-picker-item" data-value="inconsolata" data-label="Inconsolata"></span><span tabindex="0" role="button" className="ql-picker-item" data-value="ubuntu" data-label="Ubuntu Mono"></span></span></span><select className="ql-font" style={{ display: "none" }}>
                                        <option selected="">Sailec Light</option>
                                        <option value="sofia">Sofia Pro</option>
                                        <option value="slabo">Slabo 27px</option>
                                        <option value="roboto">Roboto Slab</option>
                                        <option value="inconsolata">Inconsolata</option>
                                        <option value="ubuntu">Ubuntu Mono</option>
                                        </select>
                                    </span>
                                    <span className="ql-formats me-0">
                                        <button className="ql-bold" type="button">
                                            <svg viewBox="0 0 18 18"> <path className="ql-stroke" d="M5,4H9.5A2.5,2.5,0,0,1,12,6.5v0A2.5,2.5,0,0,1,9.5,9H5A0,0,0,0,1,5,9V4A0,0,0,0,1,5,4Z"></path> <path className="ql-stroke" d="M5,9h5.5A2.5,2.5,0,0,1,13,11.5v0A2.5,2.5,0,0,1,10.5,14H5a0,0,0,0,1,0,0V9A0,0,0,0,1,5,9Z"></path> </svg></button>
                                        <button className="ql-italic" type="button"><svg viewBox="0 0 18 18"> <line className="ql-stroke" x1="7" x2="13" y1="4" y2="4"></line> <line className="ql-stroke" x1="5" x2="11" y1="14" y2="14"></line> <line className="ql-stroke" x1="8" x2="10" y1="14" y2="4"></line> </svg></button>
                                        <button className="ql-underline" type="button"><svg viewBox="0 0 18 18"> <path className="ql-stroke" d="M5,3V9a4.012,4.012,0,0,0,4,4H9a4.012,4.012,0,0,0,4-4V3"></path> <rect className="ql-fill" height="1" rx="0.5" ry="0.5" width="12" x="3" y="15"></rect> </svg></button>
                                        <button className="ql-link" type="button"><svg viewBox="0 0 18 18"> <line className="ql-stroke" x1="7" x2="11" y1="7" y2="11"></line> <path className="ql-even ql-stroke" d="M8.9,4.577a3.476,3.476,0,0,1,.36,4.679A3.476,3.476,0,0,1,4.577,8.9C3.185,7.5,2.035,6.4,4.217,4.217S7.5,3.185,8.9,4.577Z"></path> <path className="ql-even ql-stroke" d="M13.423,9.1a3.476,3.476,0,0,0-4.679-.36,3.476,3.476,0,0,0,.36,4.679c1.392,1.392,2.5,2.542,4.679.36S14.815,10.5,13.423,9.1Z"></path> </svg></button>
                                    </span>
                                    </div>
                                </div>
                                <div className="compose-footer-wrapper">
                                    <div className="btn-wrapper d-flex align-items-center">
                                    <div className="btn-group dropup me-1">
                                        <button type="button" className="btn btn-primary waves-effect waves-float waves-light">Send</button>
                                        <button type="button" className="btn btn-primary dropdown-toggle dropdown-toggle-split waves-effect waves-float waves-light" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-reference="parent">
                                        <span className="visually-hidden">Toggle Dropdown</span>
                                        </button>
                                        <div className="dropdown-menu">
                                        <Link className="dropdown-item"> Schedule Send</Link>
                                        </div>
                                    </div>
                                    <div className="email-attachement">
                                        <label for="file-input" className="form-label">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="17" height="17" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-paperclip ms-50"><path d="M21.44 11.05l-9.19 9.19a6 6 0 0 1-8.49-8.49l9.19-9.19a4 4 0 0 1 5.66 5.66l-9.2 9.19a2 2 0 0 1-2.83-2.83l8.49-8.48"></path></svg>
                                        </label>
                                        <input id="file-input" type="file" className="d-none" />
                                    </div>
                                    </div>
                                    <div className="footer-action d-flex align-items-center">
                                    <div className="dropup d-inline-block">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-more-vertical font-medium-2 cursor-pointer me-50" role="button" id="composeActions" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><circle cx="12" cy="12" r="1"></circle><circle cx="12" cy="5" r="1"></circle><circle cx="12" cy="19" r="1"></circle></svg>
                                        <div className="dropdown-menu dropdown-menu-end" aria-labelledby="composeActions">
                                        <Link className="dropdown-item">
                                            <span className="align-middle">Add Label</span>
                                        </Link>
                                        <Link className="dropdown-item">
                                            <span className="align-middle">Plain text mode</span>
                                        </Link>
                                        <div className="dropdown-divider"></div>
                                        <Link className="dropdown-item">
                                            <span className="align-middle">Print</span>
                                        </Link>
                                        <Link className="dropdown-item">
                                            <span className="align-middle">Check Spelling</span>
                                        </Link>
                                        </div>
                                    </div>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-trash font-medium-2 cursor-pointer" data-bs-dismiss="modal"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path></svg>
                                    </div>
                                </div>
                                </form>
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
  )
}

export default Content