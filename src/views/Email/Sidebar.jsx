import React from 'react'
import { Link } from 'react-router-dom'

const Sidebar = () => {
  return (
    <div className="sidebar-left">
                <div className="sidebar"><div className="sidebar-content email-app-sidebar">
                    <div className="email-app-menu">
                                <div className="sidebar-menu-list ps">
                                <div className="list-group list-group-messages ">
                                    <Link to="" className="list-group-item list-group-item-action py-1 active">
                                        <div>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-mail font-medium-3 me-50"><path d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z"></path><polyline points="22,6 12,13 2,6"></polyline></svg>
                                            <span className="align-middle">Inbox</span>
                                         </div>
                                        <span className="badge badge-light-primary rounded-pill float-end">3</span>
                                    </Link>
                                    <Link to="" className="list-group-item list-group-item-action py-1 ">
                                        <div>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-send font-medium-3 me-50"><line x1="22" y1="2" x2="11" y2="13"></line><polygon points="22 2 15 22 11 13 2 9 22 2"></polygon></svg>
                                            <span className="align-middle">Sent</span>
                                        </div>
                                        
                                    </Link>
                                    <Link to="" className="list-group-item list-group-item-action py-1">
                                        <div>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-edit-2 font-medium-3 me-50"><path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path></svg>
                                            <span className="align-middle">Draft</span>
                                        </div>
                                        <span className="badge badge-light-warning rounded-pill float-end">2</span>
                                    </Link>
                                    <Link to="" className="list-group-item list-group-item-action py-1">
                                        <div>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-star font-medium-3 me-50"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg>
                                        </div>
                                        <span className="align-middle">Starred</span>
                                    </Link>
                                    <Link to="" className="list-group-item list-group-item-action">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-info font-medium-3 me-50"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="16" x2="12" y2="12"></line><line x1="12" y1="8" x2="12.01" y2="8"></line></svg>
                                        <span className="align-middle">Spam</span>
                                        <span className="badge badge-light-danger rounded-pill float-end">5</span>
                                    </Link>
                                        <Link to="" className="list-group-item list-group-item-action">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-trash font-medium-3 me-50"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path></svg>
                                        <span className="align-middle">Trash</span>
                                    </Link>
                                </div>
                                <hr />
                                <h6 className="section-label mt-3 mb-1 px-2">Labels</h6>
                                <div className="list-group list-group-labels">
                                    <Link to="" className="list-group-item list-group-item-action"><span className="bullet bullet-sm bullet-success me-1"></span>Personal</Link>
                                    <Link to="" className="list-group-item list-group-item-action"><span className="bullet bullet-sm bullet-primary me-1"></span>Company</Link>
                                    <Link to="" className="list-group-item list-group-item-action"><span className="bullet bullet-sm bullet-warning me-1"></span>Important</Link>
                                    <Link to="" className="list-group-item list-group-item-action"><span className="bullet bullet-sm bullet-danger me-1"></span>Private</Link>
                                </div>
                                <div className="ps__rail-x" style={{ left: "0px", bottom: "0px;" }}>
                                    <div className="ps__thumb-x" tabindex="0" style={{ left: "0px", width: "0px" }}></div>
                                    </div><div className="ps__rail-y" style={{ top: "0px", height: "207px", right: "0px;" }}>
                                        <div className="ps__thumb-y" tabindex="0" style={{ top: "0px", height: "0px;" }}></div></div></div>
                            </div>
                            </div>

                                    </div>
                                    </div>
  )
}

export default Sidebar