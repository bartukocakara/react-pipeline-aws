import React from 'react'
import Sidebar from './Sidebar'
import "./app-email.min.css"
import Content from './Content'

const index = () => {
  return (
    <div className="app-content content email-application">
        <div className="content-area-wrapper container-xxl p-0">
            <Sidebar />
            <Content />
      </div>
      </div>
  )
}

export default index