import React from 'react'

export default function index() {
  return (
    <div className="app-content content ">
            <div className="content-overlay"></div>
                <div className="header-navbar-shadow"></div>
                <div className="content-body"><div className="row">
                </div>
                <section id="dashboard-analytics">
                <div class="row match-height">
                  <div class="col-lg-6 col-md-12 col-sm-12">
                    <div class="card card-congratulations">
                      <div class="card-body text-center">
                        <img src="../../../app-assets/images/elements/decore-left.png" class="congratulations-img-left" alt="card-img-left" />
                        <img src="../../../app-assets/images/elements/decore-right.png" class="congratulations-img-right" alt="card-img-right" />
                        <div class="avatar avatar-xl bg-primary shadow">
                          <div class="avatar-content">
                            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-award font-large-1"><circle cx="12" cy="8" r="7"></circle><polyline points="8.21 13.89 7 23 12 20 17 23 15.79 13.88"></polyline></svg>
                          </div>
                        </div>
                        <div class="text-center">
                          <h1 class="mb-1 text-white">Congratulations John,</h1>
                          <p class="card-text m-auto w-75">
                            You have done <strong>57.6%</strong> more sales today. Check your new badge in your profile.
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-3 col-sm-6 col-12">
                  <div class="card">
                    <div class="card-header flex-column align-items-start pb-0">
                      <div class="avatar bg-light-primary p-50 m-0">
                        <div class="avatar-content">
                          <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-users font-medium-5"><path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="9" cy="7" r="4"></circle><path d="M23 21v-2a4 4 0 0 0-3-3.87"></path><path d="M16 3.13a4 4 0 0 1 0 7.75"></path></svg>
                        </div>
                      </div>
                      <h2 class="fw-bolder mt-1">92.6k</h2>
                      <p class="card-text">Subscribers Gained</p>
                    </div>
                    <div id="gained-chart" style={{ minHeight: "100px;" }}>
                    <div id="apexchartssjzdrjd2" class="apexcharts-canvas apexchartssjzdrjd2 apexcharts-theme-light" style={{ width: "280px", height: "100px;" }}>
                    </div></div>
                  <div class="resize-triggers"><div class="expand-trigger"><div style={{ width: "281px", height: "239px" }}>
                    </div></div><div class="contract-trigger"></div></div></div>
                </div>
                  </div>
                </section>
                </div>
            </div>
  )
}
