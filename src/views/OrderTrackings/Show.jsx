import { useEffect } from 'react'
import {loadShowAction} from '../../store/actions/DataShowAction'
import {useDispatch,useSelector} from 'react-redux'
import { Link } from 'react-router-dom';

export default function Index() {

    const dispatch = useDispatch();
    const saleShow = useSelector(state => state.saleShow.saleShow);
    const location = window.location.href
    var uri = location.split("/");
    var route = uri.at(-2)
    var id = uri.at(-1)
    useEffect(() => {
        dispatch(loadShowAction(route, id));
    }, [dispatch, id, route])
    console.log( saleShow.data )
    // const showData = (key) => {
    //     return saleShow.data === null ? "No info" : saleShow.data[key]
    // }
    return (
        <div className="app-content content">
            <div className="content-overlay"></div>
            <div className="header-navbar-shadow"></div>
                <div className="content-body">
                <section className="app-user-view">
                    <div className="row">
                        <div className="row">
                            {
                                saleShow.status_code === 200  ? 
                                    <>
                                        <div className="col-lg-9 col-12 m-auto">
                                            <div className="card">
                                                <div className="card-header">
                                                    <h4 className="card-title">Order Tracking : {id}</h4>
                                                    <Link to="/crm/email" className="btn btn-primary waves-effect waves-float waves-light">Email</Link>
                                                </div>
                                                <div className="card-body">
                                                    <ul className="timeline">
                                                        <li className="timeline-item">
                                                        <span className="timeline-point">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-dollar-sign"><line x1="12" y1="1" x2="12" y2="23"></line><path d="M17 5H9.5a3.5 3.5 0 0 0 0 7h5a3.5 3.5 0 0 1 0 7H6"></path></svg>
                                                        </span>
                                                        <div className="timeline-event">
                                                            <div className="d-flex justify-content-between flex-sm-row flex-column mb-sm-0 mb-1">
                                                                <h6>Satış gerçekleşti</h6>
                                                                <span className="timeline-event-time">12 min ago</span>
                                                            </div>
                                                            <div className="d-flex justify-content-between flex-wrap flex-sm-row flex-column">
                                                                <div>
                                                                    <p className="text-muted mb-50">Email gönderildi</p>
                                                                    <div className="d-flex align-items-center">
                                                                        <div className="avatar bg-light-primary avatar-sm me-50">
                                                                            <span className="avatar-content">C</span>
                                                                        </div>
                                                                        <div className="avatar bg-light-success avatar-sm me-50">
                                                                            <span className="avatar-content">M</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div className="mt-sm-0 mt-1">
                                                                    <p className="text-muted mb-50">Tahmini Sipariş Teslim Tarihi</p>
                                                                    <p className="mb-0">06 Haziran 2022</p>
                                                                </div>
                                                                <div className="mt-sm-0 mt-1">
                                                                    <p className="text-muted mb-50">Kupon</p>
                                                                    <p className="mb-0">12.00TL</p>
                                                                </div>
                                                                <div className="mt-sm-0 mt-1">
                                                                    <p className="text-muted mb-50">Satış Toplam</p>
                                                                    <p className="mb-0">50.000 TL</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        </li>
                                                        
                                                        <li className="timeline-item">
                                                        <span className="timeline-point timeline-point-info">
                                                            <i  data-feather='list'></i>
                                                        </span>
                                                        <div className="timeline-event">
                                                            <div className="d-flex justify-content-between flex-sm-row flex-column mb-sm-0 mb-1">
                                                                <h6>Ürünler</h6>
                                                                <span className="timeline-event-time">updated 2 hours ago</span>
                                                            </div>
                                                            <button className="btn btn-outline-primary btn-sm waves-effect" type="button" data-bs-toggle="collapse" data-bs-target="#collapseExample2" aria-expanded="true" aria-controls="collapseExample2">
                                                                Ürünleri göster
                                                            </button>
                                                            <div className="collapse" id="collapseExample2">
                                                                <ul className="list-group list-group-flush mt-1">
                                                                    <li className="list-group-item d-flex justify-content-between ">
                                                                        <span>Yeşil Gömlek : </span>
                                                                        <span className="fw-bold">150.00TL</span>
                                                                        <p>2 günde aktarım</p>
                                                                    </li>
                                                                    <li className="list-group-item d-flex justify-content-between">
                                                                        <span>Siyah Pantolon : </span>
                                                                        <span className="fw-bold">320.00TL</span>
                                                                        <p>2 günde aktarım</p>
                                                                    </li>
                                                                    <li className="list-group-item d-flex justify-content-between flex-wrap">
                                                                        <span>Beyaz Çanta : </span>
                                                                        <span className="fw-bold">20.00TL</span>
                                                                        <p>2 günde aktarım</p>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        </li>
                                                        <li className="timeline-item">
                                                            <span className="timeline-point timeline-point-success">
                                                                <i data-feather='send'></i>
                                                            </span>
                                                            <div className="timeline-event">
                                                                <div className="d-flex justify-content-between flex-sm-row flex-column mb-sm-0 mb-1">
                                                                    <h6>Sipariş Teslim</h6>
                                                                    <span className="timeline-event-time">2 hours ago</span>
                                                                </div>
                                                                    <button className="btn btn-outline-primary btn-sm waves-effect" type="button" data-bs-toggle="collapse" data-bs-target="#cargo-products" aria-expanded="true" aria-controls="cargo-products">
                                                                        Kargo Durumu
                                                                    </button>
                                                                <div className="collapse w-50 m-auto" id="cargo-products">
                                                                    <ul className="list-group w-100 m-auto list-group-flush mt-1">
                                                                        <li className="list-group-item d-flex justify-content-between  flex-wrap">
                                                                            <b>Yeşil Gömlek : </b>
                                                                            <i className="text-success" data-feather='check-circle'></i>    
                                                                            <span>Zamanında Teslim</span>                                                                       
                                                                        </li>
                                                                        <li className="list-group-item d-flex justify-content-between flex-wrap">
                                                                            <b>Siyah Pantolon : </b>
                                                                            <i className="text-success" data-feather='check-circle'></i>
                                                                            <span>Zamanında Teslim</span>                                                                       
                                                                        </li>
                                                                        <li className="list-group-item d-flex justify-content-between flex-wrap">
                                                                            <b>Beyaz Çanta : </b>
                                                                            <i className="text-danger" data-feather='alert-circle'></i>
                                                                            <span className="text-danger"> <b>3</b> gün gecikme</span>   
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                            </li>
                                                    </ul>
                                                    </div>
                                                </div>
                                            </div>
                                    </>
                                    :
                                    saleShow.status_code > 300 ?
                                        saleShow.message
                                        :
                                        <div className="d-flex justify-content-center">
                                            <div className="spinner-border" role="status">
                                                <span className="sr-only"></span>
                                            </div>
                                        </div>
                            }
                        </div>
                    </div>
                </section>
            </div>
        </div>
        
    )
}