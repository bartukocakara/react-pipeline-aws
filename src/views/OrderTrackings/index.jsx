import DataTable from "./../../components/layouts/Datatable";
import Breadcrumb from "./../../components/layouts/Breadcrumb";
import { useState } from "react";

export default function Index () {

    const [status, setStatus] = useState("2");

    const handleStatus = (e) => {
        console.log(e.target.value)
        setStatus(e.target.value)
    }

    const breadcrumbList = [
        {
           label : "Order Trackings",
           path : "order-trackings",
        },
        {
            label : "List",
            path : "",
         }
    ]

    const tableActions = [
        {
            value : "email",
            icon : "mail",
            label : "Email Gönder"
        },
        {
            value : "cancel",
            icon : <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x-circle"><circle cx="12" cy="12" r="10"></circle><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg>,
            label : "İptal Et"
        }
    ]

    const buttons = [
                {
                    value : "2",
                    label : "Açık Siparişler"
                },
                {
                    value : "101",
                    label : "Gecikmeye Düşenler"
                },
                {
                    value : "102",
                    label : "Gecikmedekiler"
                },
                {
                    value : "7",
                    label : "Kargodakiler"
                },
                {
                    value : "5",
                    label : "Tamamlananlar"
                },
                {
                    value : "12",
                    label : "İade Edilenler"
                },
                {
                    value : "14",
                    label : "İptal Edilenler"
                },
            ]
    
    return (
        <div className="app-content content datatable">
            <div class="content-overlay"></div>
            <div class="header-navbar-shadow"></div>
            <Breadcrumb list={breadcrumbList} />
            <section id="basic-datatable">
                <div className="row">
                    <div className="col-12">
                    <div className="card">

                        <div id="DataTables_Table_0_wrapper" className="dataTables_wrapper dt-bootstrap5 no-footer"><div className="card-header border-bottom p-1">
                            <div className="head-label"></div>
                            
                        </div>
                        <div className="d-flex justify-content-between align-items-center mx-0 row">
                            <div className="col-sm-12 col-md-12">
                                <div id="DataTables_Table_0_filter" className="dataTables_filter">
                                    <div class="demo-inline-spacing">
                                        {
                                            buttons.map((data, i) => (
                                                <button type="button" value={data.value} key={i} class="btn btn-primary btn-md waves-effect waves-float waves-light"
                                                 onClick={(e)=> handleStatus(e)}>{data.label}</button>
                                            ))
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                        <DataTable
                                columns = {[{ "label" :  "order_info",
                                                "datas" :   ["sale_id", "create_date"]},
                                            { "label" : "customer_info",
                                                "datas" : ["firstname", "lastname", "phone", "email", "shipping_address", "billing_address"]},
                                            { "label" : "brand_info",
                                                "datas" : ["brand_id", "brand_name", "contact_email"]},
                                            {"label" : "cargo_info",
                                                "datas" : ["shipment_due", "cargo_number"]},
                                            { "label" : "product_info",
                                                "datas" : [ ["sale_item_id", "product_image", "price", "product_name", "product_id",
                                                             "price", "sold_qty", "refund_qty"]]},
                                        ]} 
                                fetchUrl = "/order-trackings"
                                tableActions ={tableActions}
                                status={status}
                        >
                        </DataTable>
                        <div className="d-flex justify-content-between mx-0 row"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="modal modal-slide-in fade" id="modals-slide-in">
                <div className="modal-dialog sidebar-sm">
                    <form className="add-new-record modal-content pt-0">
                        <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close">×</button>
                            <div className="modal-header mb-1">
                                <h5 className="modal-title" id="exampleModalLabel">New Record</h5>
                            </div>
                            <div className="modal-body flex-grow-1">
                                <div className="mb-1">
                                    <label className="form-label" for="basic-icon-default-fullname">Full Name</label>
                                    <input type="text" className="form-control dt-full-name" id="basic-icon-default-fullname" placeholder="John Doe" aria-label="John Doe" />
                                </div>
                                <div className="mb-1">
                                    <label className="form-label" for="basic-icon-default-post">Post</label>
                                    <input type="text" id="basic-icon-default-post" className="form-control dt-post" placeholder="Web Developer" aria-label="Web Developer" />
                                </div>
                                <div className="mb-1">
                                    <label className="form-label" for="basic-icon-default-email">Email</label>
                                    <input type="text" id="basic-icon-default-email" className="form-control dt-email" placeholder="john.doe@example.com" aria-label="john.doe@example.com" />
                                    <small className="form-text"> You can use letters, numbers &amp; periods </small>
                                </div>
                                <div className="mb-1">
                                    <label className="form-label" for="basic-icon-default-date">Joining Date</label>
                                    <input type="text" className="form-control dt-date flatpickr-input" id="basic-icon-default-date" placeholder="MM/DD/YYYY" aria-label="MM/DD/YYYY" readonly="readonly" />
                                </div>
                                <div className="mb-4">
                                    <label className="form-label" for="basic-icon-default-salary">Salary</label>
                                    <input type="text" id="basic-icon-default-salary" className="form-control dt-salary" placeholder="$12000" aria-label="$12000" />
                                </div>
                                <button type="button" className="btn btn-primary data-submit me-1 waves-effect waves-float waves-light">Submit</button>
                                <button type="reset" className="btn btn-outline-secondary waves-effect" data-bs-dismiss="modal">Cancel</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>
            </div>
    )
}