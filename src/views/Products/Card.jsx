const Card = () => {
    return (
            <div class="card user-card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-xl-6 col-lg-12 d-flex flex-column justify-content-between border-container-lg">
                            <div class="user-avatar-section">
                                <div class="d-flex justify-content-start">
                                    <img
                                        class="img-fluid rounded"
                                        src="../../../app-assets/images/avatars/7.png"
                                        height="104"
                                        width="104"
                                        alt="User avatar"
                                    />
                                    <div class="d-flex flex-column ms-1">
                                        <div class="user-info mb-1">
                                        <h4 class="mb-0">Eleanor Aguilar</h4>
                                        <span class="card-text">eleanor.aguilar@gmail.com</span>
                                        </div>
                                        <div class="d-flex flex-wrap">
                                        <button class="btn btn-outline-danger ms-1">Delete</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="d-flex align-items-center user-total-numbers">
                                <div class="d-flex align-items-center me-2">
                                    <div class="color-box bg-light-primary">
                                        <i data-feather="dollar-sign" class="text-primary"></i>
                                    </div>
                                <div class="ms-1">
                                    <h5 class="mb-0">23.3k</h5>
                                    <small>Monthly Sales</small>
                                </div>
                            </div>
                            <div class="d-flex align-items-center">
                                <div class="color-box bg-light-success">
                                    <i data-feather="trending-up" class="text-success"></i>
                                </div>
                                <div class="ms-1">
                                    <h5 class="mb-0">$99.87K</h5>
                                    <small>Annual Profit</small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-12 mt-2 mt-xl-0">
                        <div class="user-info-wrapper">
                            <div class="d-flex flex-wrap">
                            <div class="user-info-title">
                                <i data-feather="user" class="me-1"></i>
                                <span class="card-text user-info-title fw-bold mb-0">Username</span>
                            </div>
                            <p class="card-text mb-0">eleanor.aguilar</p>
                            </div>
                            <div class="d-flex flex-wrap my-50">
                            <div class="user-info-title">
                                <i data-feather="check" class="me-1"></i>
                                <span class="card-text user-info-title fw-bold mb-0">Status</span>
                            </div>
                            <p class="card-text mb-0">Active</p>
                            </div>
                            <div class="d-flex flex-wrap my-50">
                            <div class="user-info-title">
                                <i data-feather="star" class="me-1"></i>
                                <span class="card-text user-info-title fw-bold mb-0">Role</span>
                            </div>
                            <p class="card-text mb-0">Admin</p>
                            </div>
                            <div class="d-flex flex-wrap my-50">
                                <div class="user-info-title">
                                    <i data-feather="flag" class="me-1"></i>
                                    <span class="card-text user-info-title fw-bold mb-0">Country</span>
                                </div>
                                <p class="card-text mb-0">England</p>
                                </div>
                                <div class="d-flex flex-wrap">
                                    <div class="user-info-title">
                                        <i data-feather="phone" class="me-1"></i>
                                        <span class="card-text user-info-title fw-bold mb-0">Contact</span>
                                    </div>
                                    <p class="card-text mb-0">(123) 456-7890</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    )
}

export default Card
