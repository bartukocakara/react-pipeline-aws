const CardBody = () => {
    return (
        <div class="card-body">
            <div class="row">
                <div class="col-xl-6 col-lg-12 d-flex flex-column justify-content-between border-container-lg">
                    <div class="user-avatar-section">
                        <div class="d-flex justify-content-start">
                            <img
                                class="img-fluid rounded"
                                src="../../app-assets/images/samples/hall.jpg"
                                height="104"
                                width="104"
                                alt="User avatar"
                            />
                            <div class="d-flex flex-column ms-1">
                                <div class="user-info mb-1">
                                    <h4 class="mb-0">Bornova Basketball Hall</h4>
                                    <span class="card-text">eleanor.aguilar@gmail.com</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-12 mt-2 mt-xl-0">
                    <div class="user-info-wrapper">
                        <div class="d-flex flex-wrap">
                            <div class="user-info-title">
                                <i data-feather="user" class="me-1"></i>
                                <span class="card-text user-info-title fw-bold mb-0">Initiator of the match : </span>
                            </div>
                            <p class="card-text mb-0">Bartu Kocakara</p>
                        </div>
                        <div class="d-flex flex-wrap my-50">
                        <div class="user-info-title">
                            <i data-feather="check" class="me-1"></i>
                            <span class="card-text user-info-title fw-bold mb-0">Teams</span>
                        </div>
                        <p class="card-text mb-0">Active</p>
                        </div>
                        <div class="d-flex flex-wrap my-50">
                            <div class="user-info-title">
                                <i data-feather="flag" class="me-1"></i>
                                <span class="card-text user-info-title fw-bold mb-0">Country</span>
                            </div>
                            <p class="card-text mb-0">England</p>
                        </div>
                        <div class="d-flex flex-wrap">
                            <div class="user-info-title">
                                <i data-feather="phone" class="me-1"></i>
                                <span class="card-text user-info-title fw-bold mb-0">Contact</span>
                            </div>
                            <p class="card-text mb-0">(123) 456-7890</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default CardBody
