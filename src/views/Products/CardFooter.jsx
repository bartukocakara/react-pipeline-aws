import { Link } from "react-router-dom"
import {AiOutlineFileSearch, AiOutlineMail} from "react-icons/ai";
import {FiShare2} from "react-icons/fi";
import {GoPerson} from "react-icons/go";
import {AiOutlineWhatsApp} from "react-icons/ai";
import {TiSocialFacebook} from "react-icons/ti";
import {TiSocialTwitter} from "react-icons/ti";
import {TiSocialInstagram} from "react-icons/ti";

const CardFooter = () => {
    return (
        <div className="card-footer p-0 d-flex justify-content-between">
            <Link to="allstar-detail" className="btn btn-light">
                <AiOutlineFileSearch size={22} />
            </Link>
            <div className="d-flex">
                <Link to="match-chat" className="btn btn-light text-primary">
                    <AiOutlineMail className="card-footer-icon"/>
                </Link>
                <div class="dropdown mx-1">
                    <button class="btn btn-light text-primary" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                        <FiShare2 className="card-footer-icon" />
                    </button>
                    <ul class="dropdown-menu border" aria-labelledby="dropdownMenuButton1">
                        <li><Link class="dropdown-item text-center">With Player <GoPerson /></Link></li>
                        <li><Link class="dropdown-item text-center">Whatsapp <AiOutlineWhatsApp /></Link></li>
                        <li><Link class="dropdown-item text-center">Facebook <TiSocialFacebook /></Link></li>
                        <li><Link class="dropdown-item text-center">Twitter <TiSocialTwitter /></Link></li>
                        <li><Link class="dropdown-item text-center">Instagram <TiSocialInstagram /></Link></li>
                    </ul>
                </div>
            </div>
        </div>
    )
}

export default CardFooter
