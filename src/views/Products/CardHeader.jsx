const CardHeader = () => {
    return (
        <div class="row justify-space-between ">
            <h6 class="card-title mt-1 col-md-4">HAZ 13, PAZAR <span>19:00 - 20:00</span></h6>
            <div class="avatar-group p-1 col-md-6">
                
                    { 
                        [...Array(5)].map((avatar) => (
                        <div
                        data-bs-toggle="tooltip"
                        data-popup="tooltip-custom"
                        data-bs-placement="bottom"
                        title="Billy Hopkins"
                        class="avatar pull-up"
                        >
                            <img src="../../../app-assets/images/portrait/small/avatar-s-9.jpg" alt="Avatar" width="33" height="33" />
                        </div>
                        ))
                    }
                <h6 class="align-self-center cursor-pointer ms-50 mb-0">+7 person joining</h6>
            </div>
            <div class="text-center col-md-2 pt-2">
                <span className="badge bg-success p-1">3 person needed</span>
            </div>
        </div>
    )
}

export default CardHeader
