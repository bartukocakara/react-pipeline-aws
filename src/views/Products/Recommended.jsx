const Recommended = () => {
    return (
        <div class="card">
          <div class="card-body">
            <h5>Suggestions</h5>
            <div class="d-flex justify-content-start align-items-center mt-2">
              <div class="avatar me-75">
                <img
                  src="../../../app-assets/images/portrait/small/avatar-s-9.jpg"
                  alt="avatar"
                  height="40"
                  width="40"
                />
              </div>
              <div class="profile-user-info">
                <h6 class="mb-0">Peter Reed</h6>
                <small class="text-muted">6 Mutual Friends</small>
              </div>
              <button type="button" class="btn btn-primary btn-icon btn-sm ms-auto">
                <i data-feather="user-plus"></i>
              </button>
            </div>
            <div class="d-flex justify-content-start align-items-center mt-1">
              <div class="avatar me-75">
                <img
                  src="../../../app-assets/images/portrait/small/avatar-s-6.jpg"
                  alt="avtar img holder"
                  height="40"
                  width="40"
                />
              </div>
              <div class="profile-user-info">
                <h6 class="mb-0">Harriett Adkins</h6>
                <small class="text-muted">3 Mutual Friends</small>
              </div>
              <button type="button" class="btn btn-primary btn-sm btn-icon ms-auto">
                <i data-feather="user-plus"></i>
              </button>
            </div>
            <div class="d-flex justify-content-start align-items-center mt-1">
              <div class="avatar me-75">
                <img
                  src="../../../app-assets/images/portrait/small/avatar-s-7.jpg"
                  alt="avatar"
                  height="40"
                  width="40"
                />
              </div>
              <div class="profile-user-info">
                <h6 class="mb-0">Juan Weaver</h6>
                <small class="text-muted">1 Mutual Friends</small>
              </div>
              <button type="button" class="btn btn-sm btn-primary btn-icon ms-auto">
                <i data-feather="user-plus"></i>
              </button>
            </div>
            <div class="d-flex justify-content-start align-items-center mt-1">
              <div class="avatar me-75">
                <img
                  src="../../../app-assets/images/portrait/small/avatar-s-8.jpg"
                  alt="avatar img"
                  height="40"
                  width="40"
                />
              </div>
              <div class="profile-user-info">
                <h6 class="mb-0">Claudia Chandler</h6>
                <small class="text-muted">16 Mutual Friends</small>
              </div>
              <button type="button" class="btn btn-sm btn-primary btn-icon ms-auto">
                <i data-feather="user-plus"></i>
              </button>
            </div>
            <div class="d-flex justify-content-start align-items-center mt-1">
              <div class="avatar me-75">
                <img
                  src="../../../app-assets/images/portrait/small/avatar-s-1.jpg"
                  alt="avatar img"
                  height="40"
                  width="40"
                />
              </div>
              <div class="profile-user-info">
                <h6 class="mb-0">Earl Briggs</h6>
                <small class="text-muted">4 Mutual Friends</small>
              </div>
              <button type="button" class="btn btn-sm btn-primary btn-icon ms-auto">
                <i data-feather="user-plus"></i>
              </button>
            </div>
            <div class="d-flex justify-content-start align-items-center mt-1">
              <div class="avatar me-75">
                <img
                  src="../../../app-assets/images/portrait/small/avatar-s-10.jpg"
                  alt="avatar img"
                  height="40"
                  width="40"
                />
              </div>
              <div class="profile-user-info">
                <h6 class="mb-0">Jonathan Lyons</h6>
                <small class="text-muted">25 Mutual Friends</small>
              </div>
              <button type="button" class="btn btn-sm btn-primary btn-icon ms-auto">
                <i data-feather="user-plus"></i>
              </button>
            </div>
          </div>
        </div>
    )
}

export default Recommended
