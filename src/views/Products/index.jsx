import Card from "./Card"
import Recommended from "./Recommended"
import Search from "./Search"

const index = () => {
    return (
        <div className="app-content content ">
            <div className="content-overlay"></div>
            <div class="header-navbar-shadow"></div>
            <div className="header-navbarcontent-wrapper container-xxl p-0">
                <div className="content-header row">
                    <div className="content-body">
                        <section className="app-user-view">
                            <div class="row mt-3">
                                <div class="col-xl-9 col-lg-8 col-md-7">
                                    <Search />
                                    {
                                        [...Array(10)].map((i, match) => (
                                            <Card key={i} />
                                        ))
                                    }
                                </div>
                                <div className="col-xl-3 col-lg-4 col-md-3">
                                    <Recommended />
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default index
