import React , {useEffect}  from 'react'
import {loadUserAction} from '../../store/actions/User/ProfileActions'
import {useDispatch,useSelector} from 'react-redux'

export default function Index() {

    const dispatch = useDispatch();
    const userProfile = useSelector(state => state.userProfile.userProfile);

    useEffect(() => {
        dispatch(loadUserAction());
    }, [dispatch])
    return (
      <div id="user-profile">
          <div class="row">
              <div>
                  {
                    userProfile.status_code === 200 ?
                      <section id="profile-info">
                            <div class="row">
                              <div class="col-lg-3 col-12 order-2 order-lg-1">
                                <div class="card">
                                  <div class="card-body">
                                    <h5 class="mb-75">About</h5>
                                    <p class="card-text">
                                        {userProfile.data.first_name}
                                    </p>
                                    <div class="mt-2">
                                      <h5 class="mb-75">Joined:</h5>
                                      <p class="card-text">{userProfile.data.first_name}</p>
                                    </div>

                                    <div class="mt-2">
                                      <h5 class="mb-75">Email:</h5>
                                      <p class="card-text">{userProfile.data.email  }</p>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                        </section>
                      :
                      userProfile.status_code > 300 ?
                      userProfile.message
                      :
                      <div className="d-flex justify-content-center">
                            <div className="spinner-border" role="status">
                                <span className="sr-only"></span>
                            </div>
                        </div>
                  }
              </div>
            </div>
      </div>
    )
}
