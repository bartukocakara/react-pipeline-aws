import { useEffect } from 'react'
import {loadShowSaleAction} from '../../store/actions/SaleActions'
import {useDispatch,useSelector} from 'react-redux'

export default function Index() {

    const dispatch = useDispatch();
    const saleShow = useSelector(state => state.saleShow.saleShow);
    const location = window.location.href
    var uri = location.split("/");
    console.log(uri.at(-1))
    useEffect((uri) => {
        dispatch(loadShowSaleAction(uri.at(-1)));
    }, [dispatch])
    
    console.log( saleShow.data )

    const showData = (key) => {
        return saleShow.data === null ? "No info" : saleShow.data[key]
    }
    return (
        <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
            <section className="app-user-view">
                <div className="row">
                    <div className="col-xl-9 col-lg-8 col-md-7">
                        <div className="card user-card">
                            <div className="card-body">
                                <div className="row">
                                        <div className="col-xl-6 col-lg-12 mt-2 mt-xl-0">
                                        <div className="user-info-wrapper">
                                            <div className="d-flex flex-wrap">
                                            <p className="card-text mb-0">
                                            {
                                                saleShow.status_code === 200  ? 
                                                    <>
                                                        <div className="d-flex flex-wrap my-50">
                                                            <div className="user-info-title">
                                                                <span className="card-text user-info-title fw-bold mb-0">
                                                                    Customer Info :
                                                                </span>
                                                                <span className="card-text user-info-title fw-bold mb-0">
                                                                    {showData("firstname") + " " + showData("lastname")}
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div className="d-flex flex-wrap my-50">
                                                            <div className="user-info-title">
                                                                <span className="card-text user-info-title fw-bold mb-0">
                                                                    Sale id :
                                                                </span>
                                                                <span className="card-text user-info-title fw-bold mb-0">
                                                                    {showData("id")}
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div className="d-flex flex-wrap my-50">
                                                            <div className="user-info-title">
                                                                <span className="card-text user-info-title fw-bold mb-0">
                                                                    Sale id :
                                                                </span>
                                                                <span className="card-text user-info-title fw-bold mb-0">
                                                                    {showData("status")}
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div className="d-flex flex-wrap my-50">
                                                            <div className="user-info-title">
                                                                <span className="card-text user-info-title fw-bold mb-0">
                                                                    Installment :
                                                                </span>
                                                                <span className="card-text user-info-title fw-bold mb-0">
                                                                    {showData("installment")}
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div className="d-flex flex-wrap my-50">
                                                            <div className="user-info-title">
                                                                <span className="card-text user-info-title fw-bold mb-0">
                                                                    Email :
                                                                </span>
                                                                <span className="card-text user-info-title fw-bold mb-0">
                                                                    { showData("email")  }
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div className="d-flex flex-wrap">
                                                            <div className="user-info-title">
                                                                <span className="card-text user-info-title fw-bold mb-0">
                                                                    Payment Date :
                                                                </span>
                                                                <span className="card-text user-info-title fw-bold mb-0">
                                                                    { showData("pay_date")  }
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </>
                                                    :
                                                    saleShow.status_code > 300 ?

                                                        saleShow.message
                                                        :
                                                        <div className="d-flex justify-content-center">
                                                            <div className="spinner-border" role="status">
                                                                <span className="sr-only"></span>
                                                            </div>
                                                        </div>
                                            }
                                            </p>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    )
}