import DataTable from "./../../components/layouts/Datatable";
import Breadcrumb from "./../../components/layouts/Breadcrumb";

export default function Index () {
    
    const breadcrumbList = [
        {
           label : "Sales",
           path : "sales",
        },
        {
            label : "List",
            path : "",
         }
    ]

    const tableActions = [
        {
            value : "email",
            icon : "mail",
            label : "Email Gönder"
        },
        {
            value : "cancel",
            icon : <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x-circle"><circle cx="12" cy="12" r="10"></circle><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg>,
            label : "İptal Et"
        }
    ]
    
    return (
            <div className="app-content content datatable">
                <div class="content-overlay"></div>
                <div class="header-navbar-shadow"></div>
                <Breadcrumb list={breadcrumbList} />
                    <div className="content-body">
                        <section id="basic-datatable">
                        <div className="row">
                            <div className="col-12">
                            <div className="card">
                                <div id="DataTables_Table_0_wrapper" className="dataTables_wrapper dt-bootstrap5 no-footer"><div className="card-header border-bottom p-1">
                                    <div className="head-label">
                                        <h6 className="mb-0">Sales Table</h6>
                                    </div>
                                    <div className="dt-action-buttons text-end">
                                        <div className="dt-buttons d-inline-flex"> 
                                            <button className="dt-button buttons-collection btn btn-outline-secondary dropdown-toggle me-2" tabIndex="0" aria-controls="DataTables_Table_0" type="button" aria-haspopup="true">
                                            <span>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" className="feather feather-share font-small-4 me-50"><path d="M4 12v8a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2v-8"></path><polyline points="16 6 12 2 8 6"></polyline><line x1="12" y1="2" x2="12" y2="15"></line></svg>
                                                Export</span>
                                            </button> 
                                            </div>
                                        </div>
                                    </div>
                                    <div className="d-flex justify-content-between align-items-center mx-0 row">
                                        <div className="col-sm-12 col-md-6">
                                            <div id="DataTables_Table_0_filter" className="dataTables_filter">
                                            </div>
                                        </div>
                                    </div>

                                    <DataTable
                                        columns={ ["id", "email", "grand_total", "discount", "status", "pay_date"] }
                                        fetchUrl = "/sales"
                                        tableActions ={tableActions}
                                    >
                                    </DataTable>
                                    <div className="d-flex justify-content-between mx-0 row">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="modal modal-slide-in fade" id="modals-slide-in">
                        <div className="modal-dialog sidebar-sm">
                            <form className="add-new-record modal-content pt-0">
                                <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close">×</button>
                                <div className="modal-header mb-1">
                                    <h5 className="modal-title" id="exampleModalLabel">New Record</h5>
                                </div>
                                <div className="modal-body flex-grow-1">
                                    <div className="mb-1">
                                        <label className="form-label" for="basic-icon-default-fullname">Full Name</label>
                                        <input type="text" className="form-control dt-full-name" id="basic-icon-default-fullname" placeholder="John Doe" aria-label="John Doe" />
                                    </div>
                                    <div className="mb-1">
                                        <label className="form-label" for="basic-icon-default-post">Post</label>
                                        <input type="text" id="basic-icon-default-post" className="form-control dt-post" placeholder="Web Developer" aria-label="Web Developer" />
                                    </div>
                                    <div className="mb-1">
                                        <label className="form-label" for="basic-icon-default-email">Email</label>
                                        <input type="text" id="basic-icon-default-email" className="form-control dt-email" placeholder="john.doe@example.com" aria-label="john.doe@example.com" />
                                        <small className="form-text"> You can use letters, numbers &amp; periods </small>
                                    </div>
                                    <div className="mb-1">
                                        <label className="form-label" for="basic-icon-default-date">Joining Date</label>
                                        <input type="text" className="form-control dt-date flatpickr-input" id="basic-icon-default-date" placeholder="MM/DD/YYYY" aria-label="MM/DD/YYYY" readonly="readonly" />
                                    </div>
                                    <div className="mb-4">
                                        <label className="form-label" for="basic-icon-default-salary">Salary</label>
                                        <input type="text" id="basic-icon-default-salary" className="form-control dt-salary" placeholder="$12000" aria-label="$12000" />
                                    </div>
                                    <button type="button" className="btn btn-primary data-submit me-1 waves-effect waves-float waves-light">Submit</button>
                                    <button type="reset" className="btn btn-outline-secondary waves-effect" data-bs-dismiss="modal">Cancel</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    )
}